import React from 'react';
import './App.scss';
import Startpage from './components/startpage';
import User from './components/user';
import Admin from './components/admin';
import { Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeAuthStatus } from './actions/changeAuthStatus.action';
import { addRole } from './actions/role.action';
import { LocalStorageService } from './shared/services/local-storage.services';
import { AuthServices } from './shared/services/auth.services';


class App extends React.Component<any, any> {
  public localStorageService = new LocalStorageService;
  public authServices = new AuthServices;
  public pathName = '';

  public chekIsAuth() {
    if (!this.props.isAuth) {
      if (this.localStorageService.get('token') !== null || this.localStorageService.get('token') !== undefined) {
        this.authServices.checkRole()
          .then(res => {
            this.props.changeAuthStatus(true);
            this.props.addRole(res.data);
          })
          .catch(err => {
          })
      }
    }
    return <React.Fragment>
      <Route path="/" component={Startpage} />
    </React.Fragment>
  }

  public navigateToComponent() {
    this.chekIsAuth();
    if (this.props.isAuth) {
      switch (this.props.role) {
        case 1:
          return <React.Fragment>
            <Redirect to="/user" />
            <Route path="/user" component={User} />
          </React.Fragment>
        case 2:
          return <React.Fragment>
            <Redirect to="/admin" />
            <Route path="/admin" component={Admin} />
          </React.Fragment>
      }
    }
    return <React.Fragment>
      <Redirect to="/" />
      <Route path="/" component={Startpage} />
    </React.Fragment>
  }

  public render() {
    return (
      <div>
        <Switch>
          {this.navigateToComponent()}
        </Switch>
      </div>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    isAuth: state.isAuth,
    role: state.role,
  }
}

function matchDispatchToProps(dispatch: any) {
  return bindActionCreators({ changeAuthStatus: changeAuthStatus, addRole: addRole }, dispatch)
}
export default connect(mapStateToProps, matchDispatchToProps)(App);

