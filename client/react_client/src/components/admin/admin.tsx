import * as React from 'react';
import { bindActionCreators } from 'redux';
import { addRole } from '../../actions/role.action';
import { changeAuthStatus } from '../../actions/changeAuthStatus.action';
import { connect } from 'react-redux';

class Admin extends React.Component<any, any>  {

  constructor(props: any){
    super(props);
  }
  
  public render() {
    return (
      <div>
        Admin
      </div>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    isAuth: state.isAuth,
  }
}

function matchDispatchToProps(dispatch: any) {
  return bindActionCreators({ changeAuthStatus: changeAuthStatus, addRole: addRole }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Admin);

