import * as React from 'react';
import './user-header.scss';
import UserChangeForm from './user-change-form';
import { LocalStorageService } from '../../../shared/services/local-storage.services';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeUserFormStatus } from '../../../actions/isUserChangeForm.action';
import { PagingModel } from '../../../shared/models/paging.models';
import { Link } from 'react-router-dom';
import { changeAuthStatus } from '../../../actions/changeAuthStatus.action';

class UserHeader extends React.Component<any, any>  {
    public localStorageService = new LocalStorageService;
    public pagingCounter: PagingModel = {
        lengthArr: 0,
        start: 0,
        step: 4,
        quantityBtn: 0
    }

    public logout() {
        this.localStorageService.remove('token');
        this.props.changeAuthStatus(false);
    }
    
    
    public render() {
        return (
            <header className="header">
                <div className="user-info">
                    <nav className="user-nav">
                        <p className="user-name"><i className="fas fa-user"></i>User Name</p>
                        <ul>
                            <li onClick={() => this.props.changeUserFormStatus(!this.props.isUserChangeForm)}>Change user info</li>
                            <li ><Link to="/user">Search books</Link></li>
                        </ul>
                    </nav>
                    <button
                        className="user-logout btn btn-danger"
                        onClick={() => this.logout()}
                    ><i className="fas fa-sign-out-alt"></i> Log out</button>
                </div>
                {(this.props.isUserChangeForm) ? <UserChangeForm /> : null}
            </header>
        );
    }
}

function mapStateToProps(state: any) {
    return {
        isAuth: state.isAuth,
        isUserChangeForm: state.isUserChangeForm
    }
}

function matchDispatchToProps(dispatch: any) {
    return bindActionCreators({
        changeUserFormStatus: changeUserFormStatus,
        changeAuthStatus: changeAuthStatus
    }, dispatch)
}
export default connect(mapStateToProps, matchDispatchToProps)(UserHeader);

