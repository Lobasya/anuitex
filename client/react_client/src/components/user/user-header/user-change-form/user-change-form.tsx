import * as React from 'react';
import './user-change-form.scss';
import { changeUserFormStatus } from '../../../../actions/isUserChangeForm.action';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { UserServices } from '../../../../shared/services/user.services';
import { IUserModel } from '../../../../shared/models/user.model';
import jwtDecode from 'jwt-decode';
import { LocalStorageService } from '../../../../shared/services/local-storage.services';

class UserChangeForm extends React.Component<any, any>  {
    public userServices = new UserServices;
    public localStorageService = new LocalStorageService

    public userFromToken: any = jwtDecode(this.localStorageService.get('token'));
    

    public userForm: IUserModel = {
        _id: this.userFromToken.id,
        userName:  this.userFromToken.userName,
        email:  this.userFromToken.email,
        password: '',
        conf: true
      };

    public change(){
        this.userServices.edit(this.userForm).then((res: any) =>{
            console.log(res.data)
        })
    }

    public render() {
        console.log(this.userFromToken)
        return (
            <div className="container change-user" >
                <form className="form" onSubmit={() => this.change()}>
                    <div className="form-group">
                        <label htmlFor="inputName">User name</label>
                        <input 
                        type="text" 
                        className="form-control" 
                        id="inputName" 
                        aria-describedby="emailHelp" 
                        placeholder="Enter user name" 
                        onChange={(event) => this.userForm.userName = event.target.value}
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="inputEmail">Email address</label>
                        <input 
                        type="email" 
                        className="form-control" 
                        id="exampleInputEmail1" 
                        placeholder="Enter email" 
                        onChange={(event) => this.userForm.email = event.target.value}
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="inputPass">Password</label>
                        <input 
                        type="password" 
                        className="form-control" 
                        id="inputPass"
                        placeholder="Enter password" 
                        onChange={(event) => this.userForm.password = event.target.value}
                        />
                    </div>

                    <button type="submit" className="btn btn-primary">Save</button>
                </form>
                <div className="logouot">
                    <button 
                    className="logout-btn"
                    onClick = {() => this.props.changeUserFormStatus(!this.props.isUserChangeForm)}
                    >X</button>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state: any) {
    return {
        isUserChangeForm: state.isUserChangeForm,
    }
  }
  
  function matchDispatchToProps(dispatch: any) {
    return bindActionCreators({ changeUserFormStatus: changeUserFormStatus }, dispatch)
  }
  export default connect(mapStateToProps, matchDispatchToProps)(UserChangeForm);
  
