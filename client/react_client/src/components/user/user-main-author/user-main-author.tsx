import * as React from 'react';
import './user-mian-author.scss';
import AuthorImg from '../../../img/author.png';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { LocalStorageService } from '../../../shared/services/local-storage.services';
import { changeAuthStatus } from '../../../actions/changeAuthStatus.action';
import { Link } from 'react-router-dom';
import { addUserAuthorById } from '../../../actions/user-author.action';
import { AuthorServices } from '../../../shared/services/author.services';
import { BooksServices } from '../../../shared/services/books.services';

class UserMainAuthor extends React.Component<any, any>  {
    public localStorageService = new LocalStorageService;
    public authorServices = new AuthorServices;
    public booksServices = new BooksServices;
    public newAuthor = {};

    public getAuthor() {
        if (!this.props.location.query) {
            return;
        }
        let id = this.props.location.query.authorId;
        this.authorServices.getAuthorById(id).then((resBook: any) => {
            let authorObj = resBook.data;
            this.booksServices.getByAuthorId(id).then((resAuthor: any) => {
                authorObj.books = resAuthor.data;
                this.newAuthor = authorObj;
                this.props.addUserAuthorById(authorObj);
            })

        })
    }

    public render() {
        if (this.props.userAuthorById !== this.newAuthor) {
            this.getAuthor();
        }
        return (
            <main>
                <section className="container author-block">
                    <div className="author-info">
                        <img src={(!this.props.userAuthorById.img) ? AuthorImg : this.props.userAuthorById.img} alt="avatar author" />
                        <h1>{this.props.userAuthorById.name}</h1>
                        <p>Birth date: <span>18.07.2019</span></p>
                        <p>Biography:
                        <span>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id vel sint officia architecto a. Pariatur
                                reprehenderit voluptatum eligendi aspernatur ipsum non molestias dolorum, eos eveniet laboriosam nulla ad sunt
                                culpa.
                        </span>
                        </p>
                    </div>
                    <div className="author-books">
                        <h2>All author books</h2>
                        <ul>
                            {
                                [].map.call(this.props.userAuthorById.books, (item: any, index: any) => {
                                    return <li key={index}>
                                        <Link                                           
                                            to={{
                                                pathname: "/user/book/" + item._id,
                                                query: { bookId: item._id }
                                            }}>

                                            {item.title},

                                        </Link>
                                    </li>
                                })
                            }
                        </ul>
                    </div>
                </section>
            </main>
        );
    }
}

function mapStateToProps(state: any) {
    return {
        userAuthorById: state.userAuthorById
    }
}

function matchDispatchToProps(dispatch: any) {
    return bindActionCreators({
        changeAuthStatus: changeAuthStatus,
        addUserAuthorById: addUserAuthorById
    }, dispatch)
}
export default connect(mapStateToProps, matchDispatchToProps)(UserMainAuthor);

