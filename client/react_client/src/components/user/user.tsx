import * as React from 'react';
import UserHeader from './user-header';
import { Route, Switch } from 'react-router-dom';
import UserMainSearch from './user-main-search';
import UserMainBook from './user-main-book';
import UserMainAuthor from './user-main-author';


class User extends React.Component<any, any>  {
  public render() {
    return (
      <div>
        <UserHeader />
        <Switch>
          <Route path='/user' component={UserMainSearch} exact></Route>
          <Route path='/user/book' component={UserMainBook}></Route>
          <Route path='/user/author/:authorId' component={UserMainAuthor}></Route>
        </Switch>
      </div>
    );
  }
}

export default User;
