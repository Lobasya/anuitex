import * as React from 'react';
import './user-mian-book.scss';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { LocalStorageService } from '../../../shared/services/local-storage.services';
import { changeAuthStatus } from '../../../actions/changeAuthStatus.action';
import { BooksServices } from '../../../shared/services/books.services';
import { addUserBookById } from '../../../actions/user-book.action';
import { Link } from 'react-router-dom';

class UserMainBook extends React.Component<any, any>  {
    public localStorageService = new LocalStorageService;
    public bookServices = new BooksServices;
    public newBook = {};

    public getBook() {
        if(!this.props.location.query){
            return;
        }
        let id = this.props.location.query.bookId;
        this.bookServices.getById(id).then((res: any) => {
            this.newBook = res.data[0];
            this.props.addUserBookById(res.data[0]);
        })
    }

    public render() {
        if (this.props.userBookById !== this.newBook) {
            this.getBook();
        }
        return (
            <main>
                <section className="container show-book">
                    <div className="user-book-info">
                        <img src={this.props.userBookById.img} alt="book img" />
                        <div className="info">
                            <h1 className="user-book-title">{this.props.userBookById.title}</h1>
                            <p className="user-book-author">Автор: {
                                [].map.call(this.props.userBookById.authorId, (item: any, index: any) => {
                                    return <Link
                                        key={index}
                                        to={{
                                            pathname: "/user/author/" + item._id,
                                            query: { authorId: item._id }
                                        }} >
                                         {item.name} , 
                                    </Link>
                                })
                            }</p>
                            <p className="user-book-language"><span>Language:</span> Russian</p>
                            <p className="user-book-short">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem repudiandae quod,
                              consequuntur excepturi nulla veniam laudantium asperiores nihil voluptatem accusamus. In repudiandae perferendis
                              ad, magni maiores possimus facere nobis quae veniam iusto nam impedit nulla eaque harum atque consequatur
                              deleniti voluptatibus hic soluta cumque rerum ipsum sed! Tenetur, aspernatur ipsum. Deleniti quis dolor
                              voluptate cupiditate aspernatur reiciendis ipsa, a ea eum magni possimus laborum expedita omnis, esse ullam
                              voluptatum, reprehenderit consequatur odio. Nihil, rerum dolorem nisi voluptatum ipsa, non facere, itaque ex
                              ullam accusamus sed doloribus. Fuga voluptatibus esse ipsam at ducimus, alias dolor maiores. Repellat
                                    voluptatibus dolores a eius! </p>
                            <p className="user-book-length"><span>Length:</span> 128 pages</p>
                            <p className="user-book-length"><span>File Size:</span> 1068 KB</p>
                            <p className="user-book-length"><span>Publication date:</span>1997</p>
                            <div className="social">
                                <span className="social-span">Follow us:</span>
                                <i className="fab fa-twitter"></i>
                                <i className="fab fa-facebook-square"></i>
                                <i className="fab fa-google-plus-g"></i>
                            </div>
                        </div>
                    </div>
                    <div className="user-book-text">
                        <h1>Short type</h1>
                        <p className="user-book-parag">p</p>
                    </div>
                </section>
            </main>
        );
    }
}

function mapStateToProps(state: any) {
    return {
        isAuth: state.isAuth,
        userBookById: state.userBookById
    }
}

function matchDispatchToProps(dispatch: any) {
    return bindActionCreators({
        changeAuthStatus: changeAuthStatus,
        addUserBookById: addUserBookById
    }, dispatch)
}
export default connect(mapStateToProps, matchDispatchToProps)(UserMainBook);

