import * as React from 'react';
import './user-mian-search.scss';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { LocalStorageService } from '../../../shared/services/local-storage.services';
import { changeAuthStatus } from '../../../actions/changeAuthStatus.action';
import { BooksServices } from '../../../shared/services/books.services';
import { PagingModel } from '../../../shared/models/paging.models';
import ReactPaginate from 'react-paginate';
import { addbooksPagination } from '../../../actions/booksArrPagination.action';
import { changePaginationCounter } from '../../../actions/changePaginationCounter.action';
import { Link } from 'react-router-dom';

class UserMainSearch extends React.Component<any, any>  {
    public localStorageService = new LocalStorageService;
    public bookServices = new BooksServices;

    public arrElementsOfBook: any = [];

    public pagingCounter: PagingModel = {
        lengthArr: 0,
        start: 0,
        step: 4,
        quantityBtn: 0
    }

    public searchVal = '';

    public search(event: any) {
        if (typeof event.target.value == 'string') {
            if (event.target.value.length < 1) {
                this.getPaging();
            }
            this.searchVal = event.target.value;
        }
        this.bookServices.search('', event.target.value, this.pagingCounter).then(res => {
            this.pagingCounter.lengthArr = res.data.booksLength;
            this.arrElementsOfBook = res.data.booksArr;
            this.props.addbooksPagination(res.data.booksArr);
        })
    }

    public getPaging(): void {
        this.bookServices.get().then((resGet: any) => {
            this.pagingCounter.lengthArr = resGet.data.length;
            this.bookServices.getPaging(this.pagingCounter).then((resGetPaging: any) => {
                this.arrElementsOfBook = resGetPaging.data;
                this.props.addbooksPagination(resGetPaging.data);
            })
        })
    }

    public handlePageClick(data: any) {
        this.pagingCounter.start = data.selected;
        this.getPaging();
    }

    public render() {
        if (this.arrElementsOfBook !== this.props.booksArrPagination) {
            this.getPaging();
        }
        return (
            <main>
                <section className="container user-serach-book">
                    <div className="addbook-sort row col-lg-12">
                        <div className="form-group col-lg-12">
                            <input
                                type="email"
                                className="form-control"
                                id="addBookName"
                                placeholder="Search..."
                                onChange={() => this.search(event)}
                            />
                        </div>
                    </div>
                </section>
                <section className="books container user-serach-book">
                    <div className="row col-lg-12">
                        {[].map.call(this.props.booksArrPagination, (item: any, index: any) => {
                            return <div className="card" key={item._id}>
                                <div className="card-body-img">
                                    <img className="card-img-top" src={item.img} alt="Card image cap" />
                                </div>
                                <div className="card-body">
                                    <h5 className="card-title">{item.title}</h5>
                                    <p className="card-text">Authors: {
                                        item.authorId.map((author: any, index: any) => {
                                            return (<Link
                                                key={index}
                                                to={{ pathname: "/user/author/" + author._id, query: { authorId: author._id } }}>
                                                {author.name},
                                                </Link>)
                                        })
                                    }</p>
                                    <Link
                                        key={index}
                                        to={{ pathname: "/user/book/" + item._id, query: { bookId: item._id } }} >
                                        <button className="btn btn-primary" >Open</button>
                                    </Link>
                                </div>
                            </div>
                        })}
                    </div >

                    <ReactPaginate
                        previousLabel={'previous'}
                        nextLabel={'next'}
                        breakLabel={'...'}
                        breakClassName={'break-me'}
                        onPageChange={this.handlePageClick.bind(this)}
                        pageCount={Math.ceil(this.pagingCounter.lengthArr / this.pagingCounter.step)}
                        marginPagesDisplayed={this.pagingCounter.step}
                        pageRangeDisplayed={4}
                        containerClassName={'pagination'}
                        activeClassName={'active'}
                    ></ReactPaginate>
                </section >
            </main>
        );
    }
}

function mapStateToProps(state: any) {
    return {
        booksArrPagination: state.booksArrPagination,
        pagingCounter: state.paginationCounter,
    }
}

function matchDispatchToProps(dispatch: any) {
    return bindActionCreators({
        changeAuthStatus: changeAuthStatus,
        addbooksPagination: addbooksPagination,
        changePaginationCounter: changePaginationCounter,
    }, dispatch)
}
export default connect(mapStateToProps, matchDispatchToProps)(UserMainSearch);
