import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { startPageState } from '../../actions/startPageState.action';
import './startpage.scss';
import Registration from './registration';
import Login from './login';

class Startpage extends React.Component<any, any>  {

  public render() {
    return (
      <section className="start-page">
        <div className="main-form">
          <button
            className={(this.props.startPage) ? "btn-singup btn-login" : "btn-singup"}
            onClick={() => this.props.startPageState(!this.props.startPage)}
          >+
          </button>
          {(this.props.startPage) ? <Registration /> : <Login />}
        </div>
      </section>
    );
  }

}

function mapStateToProps(state: any) {
  return {
    startPage: state.startPageState,
  }
}

function matchDispatchToProps(dispatch: any) {
  return bindActionCreators({ startPageState: startPageState }, dispatch)
}
export default connect(mapStateToProps, matchDispatchToProps)(Startpage);

