import * as React from 'react';
import './registration.scss';
import { AuthServices } from '../../../shared/services/auth.services';
import { LocalStorageService } from '../../../shared/services/local-storage.services';
import { IUserModel } from '../../../shared/models/user.model';

class Registration extends React.Component<any, any>  {
  constructor(props: any, 
    private authServices: AuthServices, 
    private localStorageService: LocalStorageService) {
    super(props);
    this.authServices = new AuthServices;
    this.localStorageService = new LocalStorageService;
  }

  public formObj: IUserModel = {
    userName: '',
    email: '',
    password: ''
  }

  public handleChange(event: any) {
    if (event.target.name == "userName") {
      this.formObj.userName = event.target.value;
      return;
    }
    if (event.target.name == "email") {
      this.formObj.email = event.target.value;
      return;
    }
    if (event.target.name == "password") {
      this.formObj.password = event.target.value;
      return;
    }
  }

  public login(event: any) {
    event.preventDefault();
    console.log(this.formObj)
    this.authServices.registration(this.formObj)
      .then(res => {
        console.log(res);
      })
      .catch(err => console.log(err));
    
  }
  public render() {
    return (
      <form className="reg register-state" onSubmit={(event) => this.login(event)}>
        <span>register</span>

        <input 
        type="text" 
        placeholder="User name" 
        name="userName" 
        onChange={() => this.handleChange(event)}
        />

        <input 
        type="email" 
        placeholder="E-Mail" 
        name="email" 
        pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" 
        onChange={() => this.handleChange(event)}
        />

        <input 
        type="text" 
        placeholder="Password" 
        name="password" 
        onChange={() => this.handleChange(event)}
        />

        <button type="submit" className="btn-send">Sign </button>
        <p className="status">Incorrect e-mail</p>
      </form>
    );
  }
}

export default Registration;
