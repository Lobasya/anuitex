import * as React from 'react';
import { IUserModel } from '../../../shared/models/user.model';
import { AuthServices } from '../../../shared/services/auth.services';
import { LocalStorageService } from '../../../shared/services/local-storage.services';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addRole } from '../../../actions/role.action';
import { changeAuthStatus } from '../../../actions/changeAuthStatus.action';

class Login extends React.Component<any, any>  {

  public authServices = new AuthServices;
  public localStorageService = new LocalStorageService;

  public formObj: IUserModel = {
    email: '',
    password: ''
  }

  public handleChange(event: any) {
    if (event.target.name == "userName") {
      this.formObj.email = event.target.value;
      return;
    }
    if (event.target.name == "password") {
      this.formObj.password = event.target.value;
      return;
    }
  }

  public login(event: any) {
    event.preventDefault();
    this.authServices.login(this.formObj)
      .then(res => {
        this.localStorageService.set('token', res.data.token);
        this.props.changeAuthStatus(true);
        this.props.addRole(res.data.user.role)
      })
      .catch(err => console.log(err));
    
  }
  public render() {
    return (
      <form className="login" onSubmit={(event) => this.login(event)}>
        <span>login</span>

        <input
          type="text"
          placeholder="E-Mail"
          name="userName"
          pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
          onChange={() => this.handleChange(event)}
        />

        <input
          type="text"
          placeholder="Password"
          name="password"
          onChange={() => this.handleChange(event)}
        />

        <button type="submit" className="btn-send">Log</button>
        <p className="status">status</p>
      </form>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    role: state.role,
    isAuth: state.isAuth
  }
}

function matchDispatchToProps(dispatch: any) {
  return bindActionCreators({ addRole: addRole, changeAuthStatus: changeAuthStatus }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Login);

