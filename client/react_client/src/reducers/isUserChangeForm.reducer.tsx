export default function(state = false, action: any){
    switch (action.type) {
        case "IS_USER_CHANGE_FORM":
            return action.payload
        
        default:
            return state;
    }
}