const defaultBook = {
    authorId: [
        {
            _id: '123',
            name: 'Author',
            img: 'img'
        },
    ],
    date: new Date,
    img: "http://books.google.com/books/content?id=lomDOyliLJsC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api",
    length: 197,
    magazine: ["5c496f0739d12806908ebcaa"],
    title: "Quilt Culture",
    _id: "5c598acb55383b1494e522da",
}

export default function(state = defaultBook, action: any){
    switch (action.type) {
        case "USER_BOOK_BY_ID":
            return action.payload
        
        default:
            return state;
    }
}