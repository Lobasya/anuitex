export default function(state = false, action: any){
    switch (action.type) {
        case "START_PAGE":
            return action.payload
        
        default:
            return state;
    }
}