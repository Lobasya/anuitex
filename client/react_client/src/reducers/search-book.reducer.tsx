export default function(state = '', action: any){
    switch (action.type) {
        case "SEARCH_BOOK":
            return action.payload
        
        default:
            return state;
    }
}