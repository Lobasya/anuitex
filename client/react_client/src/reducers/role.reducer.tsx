export default function(state = 0, action: any){
    switch (action.type) {
        case "SET_ROLE":
            return action.payload
        
        default:
            return state;
    }
}