const defaultAuthor = {
    _id: '123',
    name: 'Name',
    img: 'img',
    books: [
        {
            _id: '123',
            title: 'Name'
        }
    ]
}

export default function(state = defaultAuthor, action: any){
    switch (action.type) {
        case "USER_AUTHOR_BY_ID":
            return action.payload
        
        default:
            return state;
    }
}