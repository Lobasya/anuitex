export default function(state: any = [], action: any){
    switch (action.type) {
        case "BOOKS_ARRAY_PAGINATION":
            return action.payload
        
        default:
            return state;
    }
}