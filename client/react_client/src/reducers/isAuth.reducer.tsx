export default function(state = false, action: any){
    switch (action.type) {
        case "CHANGE_AUTH":
            return action.payload
        
        default:
            return state;
    }
}