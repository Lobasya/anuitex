import { PagingModel } from "../shared/models/paging.models";

const pagingCounter: PagingModel = {
    lengthArr: 0,
    start: 0,
    step: 4,
    quantityBtn: 0
}

export default function(state: PagingModel = pagingCounter, action: any){
    switch (action.type) {
        case "PAGINATION_COUNTER":
            return action.payload
        
        default:
            return state;
    }
}