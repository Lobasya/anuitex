import {combineReducers} from 'redux';
import StartPageState  from './startPageState.reducer';
import GetRole  from './role.reducer';
import AuthStatus  from './isAuth.reducer';
import isUserChangeForm from './isUserChangeForm.reducer';
import booksArrPagination from './booksArrPagination.reducer';
import paginationCounter from './paginationCounter.reducer';
import userBookById from './user-book.reducer';
import userAuthorById from './user-author.reducer';
import searchBook from './search-book.reducer';


const allReducers = combineReducers({
    startPageState: StartPageState,
    isAuth: AuthStatus,
    role: GetRole,  
    isUserChangeForm: isUserChangeForm,
    booksArrPagination: booksArrPagination,
    paginationCounter: paginationCounter,
    userBookById: userBookById,
    userAuthorById: userAuthorById,
    searchBook: searchBook,
});

export default allReducers;