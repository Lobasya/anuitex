import { IUserModel } from "../models/user.model";
import axios from "axios";
import { environment } from '../../environments/environment';
import AuthInterceptor from "../interceptors/auth.interceptor";

export class AuthServices extends AuthInterceptor{

  public login(user: IUserModel) {
    const body = { 
      email: user.email, 
      password: user.password };
    return axios.post(environment.serverUrl + '/auth/login', body);
  }

  public registration(user: IUserModel) {
    const body = {
      userName: user.userName,
      email: user.email,
      password: user.password,
      conf: false
    };
    return axios.post(environment.serverUrl + '/auth/register', body);
  }

  public checkRole() {
    return axios.get(environment.serverUrl + '/auth/user-role');
  }

}
