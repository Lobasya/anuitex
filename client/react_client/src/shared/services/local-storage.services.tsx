export class LocalStorageService {

    public get<Type>(localKey: string): Type {
        let item: any = localStorage.getItem(localKey);
        return JSON.parse(item);
    }

    public set(name: string, item: any): void {
        let saveditem = JSON.stringify(item);
        return localStorage.setItem(name, saveditem);
    }

    public remove(localKey: string): void {
        localStorage.removeItem(localKey);
    }
}
