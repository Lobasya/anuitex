import { IUserModel } from "../models/user.model";
import axios from "axios";
import { environment } from '../../environments/environment';
import AuthInterceptor from "../interceptors/auth.interceptor";

export class UserServices extends AuthInterceptor{

  public edit(user: IUserModel) {
    const body: IUserModel = {
      _id: user._id,
      userName: user.userName,
      email: user.email,
      password: user.password,
      conf: true
    };
    return axios.post(environment.serverUrl + '/user', body);
  }

}
