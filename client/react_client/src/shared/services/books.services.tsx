import axios from "axios";
import { environment } from '../../environments/environment';
import AuthInterceptor from "../interceptors/auth.interceptor";
import { PagingModel } from "../models/paging.models";
import { any } from "prop-types";

export class BooksServices extends AuthInterceptor{

  public get(): any {
    return axios.get(environment.serverUrl + '/book');
  }

  public getPaging(pagingCounter: PagingModel): any {
    const body = { start: pagingCounter.start, step: pagingCounter.step };
    return axios.post(environment.serverUrl + '/book/paging', body);
  }

  public getById(id: string): any {
    return axios.get(environment.serverUrl + '/book/' + id);
  }

  public getByAuthorId(id: string) {
    return axios.get(environment.serverUrl + '/book/author/' + id);
  }

  public search(id: string, title: string, paging: any) {
    let body = { 
      id: id, 
      title: title, 
      start: paging.start, 
      step: paging.step 
    };
    return axios.post(environment.serverUrl + '/book/search', body);
  }

}
