import axios from "axios";
import { environment } from '../../environments/environment';
import AuthInterceptor from "../interceptors/auth.interceptor";

export class AuthorServices extends AuthInterceptor{

  public getAuthorById(authorId: string) {
    return axios.get(environment.serverUrl + '/author/' + authorId);
  }

}
