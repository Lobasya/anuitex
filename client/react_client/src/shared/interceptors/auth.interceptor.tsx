import axios from "axios";

export default class AuthInterceptor {
    
    constructor(){
        this.authHeader();
    }

    public authHeader() {
        var token: any = localStorage.getItem('token');
        if(token !== null){
            return axios.defaults.headers.common['x-access-token'] = JSON.parse(token);
        }
        return;
      }
}