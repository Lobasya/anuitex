export interface IBookModel {
    title: String,
    img: any,
    authorId: string[],
    date: Date,
    length: Number,
    magazine: String[],
    _id?: String,
}