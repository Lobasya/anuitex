import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './index.scss';
import App from './App';
import { createStore } from 'redux';
import allReducers from './reducers';
import { Router, Switch } from 'react-router-dom';
import createHistory from "history/createBrowserHistory";

const store = createStore(allReducers);

ReactDOM.render(
    <Provider store={store}>
        <Router history={createHistory()}>
            <Switch >
                <App />
            </Switch>
        </Router>
    </Provider>,
    document.getElementById('root'));


