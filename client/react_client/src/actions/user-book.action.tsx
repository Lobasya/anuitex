import { IBookModel } from "../../../../shared/models";

export const addUserBookById = (book: IBookModel) =>{
    return {
        type: "USER_BOOK_BY_ID",
        payload: book
    }
};