export const changeAuthStatus = (isAuth: boolean) =>{
    return {
        type: "CHANGE_AUTH",
        payload: isAuth
    }
};