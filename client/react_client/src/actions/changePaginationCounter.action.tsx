import { PagingModel } from "../shared/models/paging.models";

export const changePaginationCounter = (paginationCounter: PagingModel) =>{
    return {
        type: "PAGINATION_COUNTER",
        payload: paginationCounter
    }
};