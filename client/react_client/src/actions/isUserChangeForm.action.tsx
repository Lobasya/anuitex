export const changeUserFormStatus = (isUserChangeForm: boolean) =>{
    return {
        type: "IS_USER_CHANGE_FORM",
        payload: isUserChangeForm
    }
};