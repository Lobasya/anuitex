export const addbooksPagination = (booksArr: []) =>{
    return {
        type: "BOOKS_ARRAY_PAGINATION",
        payload: booksArr
    }
};