import { IAuthorModel } from "../../../../shared/models";

export const addUserAuthorById = (author: IAuthorModel) =>{
    return {
        type: "USER_AUTHOR_BY_ID",
        payload: author
    }
};