export const addRole = (role: number) =>{
    return {
        type: "SET_ROLE",
        payload: role
    }
};