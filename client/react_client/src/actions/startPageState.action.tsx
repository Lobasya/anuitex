export const startPageState = (isState: boolean) =>{
    return {
        type: "START_PAGE",
        payload: isState
    }
};