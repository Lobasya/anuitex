
export const addSearchBook = (search: string) =>{
    return {
        type: "SEARCH_BOOK",
        payload: search
    }
};