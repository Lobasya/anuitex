import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { StartPageComponent } from './component/start-page/start-page.component';
import { AdminGuardGuard } from './shared/guards/admin-guard.guard';
import { UserGuardGuard } from './shared/guards/user-guard.guard';
import { AuthServices } from './shared/services/auth.service';
import { BookServices } from './shared/services/book.services';
import { AuthorServices } from './shared/services/author.services';
import { MagazineServices } from './shared/services/magazine.services';
import { HeaderInterceptor } from './shared/interceptor/head.interceptor';
import { PreloaderModule } from './component/preloader/preloader.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 

@NgModule({
  declarations: [
    AppComponent,
    StartPageComponent,
 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    PreloaderModule,
    MatFormFieldModule,
    MatSelectModule,
    BrowserAnimationsModule
  ],
  providers: [ 
    AdminGuardGuard, 
    UserGuardGuard, 
    AuthServices,
    BookServices,
    AuthorServices,
    MagazineServices,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HeaderInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
