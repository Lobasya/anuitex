import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { LocalStorageService } from './local-storage.service';
import { Observable } from 'rxjs';
import { IUserModel } from '../../../../../../shared/models/user.model';

@Injectable()

export class AuthServices {

  constructor(private http: HttpClient,
    private localStorageService: LocalStorageService) { }

  public registration(user: IUserModel): Observable<Object> {
    const body = { 
      userName: user.userName, 
      email: user.email, 
      password: user.password, 
      conf: user.conf 
    };
    return this.http.post(environment.serverUrl + '/auth/register', body);
  }

  public login(user: IUserModel): Observable<Object> {
    const body = { email: user.email, password: user.password };
    return this.http.post(environment.serverUrl + '/auth/login', body);
  }

  public userAuth(): Boolean {
    if (this.localStorageService.get('user')['token'] !== null) {
      if (!this.localStorageService.get('user')['token'].auth) {
        return false;
      }
      return true
    }
    return false;
  }

}
