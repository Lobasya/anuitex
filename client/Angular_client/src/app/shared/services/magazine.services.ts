import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { MagazineModel } from '../models/magazine.models';
import { PagingModel } from '../models/paging.models';

@Injectable()

export class MagazineServices {

  constructor(private http: HttpClient) { }

  public get(): Observable<Object> {
    return this.http.get(environment.serverUrl + '/magazine');
  }

  public getPaginate(pagingCounter: PagingModel): Observable<Object> {
    const body = { start: pagingCounter.start, step: pagingCounter.step };
    return this.http.post(environment.serverUrl + '/magazine/paging', body);
  }

  public getById(id: string): Observable<Object> {
    return this.http.get(environment.serverUrl + '/magazine/' + id);
  }
  

  public search(id: string, title: string, paging: any): Observable<Object> {
    let body = {
      id: id,
      title: title,
      start: paging.start,
      step: paging.step
    };
    return this.http.post(environment.serverUrl + '/magazine/search', body);
  }

  public add(magazine: MagazineModel): Observable<Object> {
    let body: MagazineModel = { title: magazine.title, img: magazine.img }
    return this.http.post(environment.serverUrl + '/magazine', body);
  }
  
  public update(magazine: MagazineModel, id: string): Observable<Object> {
    return this.http.put(environment.serverUrl + '/magazine/'+ id, magazine);
  }
  
  public delete(id: string): Observable<Object> {
    return this.http.delete(environment.serverUrl + '/magazine/' + id);
  }

}
