import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { BookModel } from '../models/books.models';
import { WordsForGenerator } from '../models/book-generator';
import { PagingModel } from '../models/paging.models';

@Injectable()

export class BookServices {
  public lipsumwords: String[] = WordsForGenerator;

  constructor(private http: HttpClient) { }

  public get(): Observable<Object> {
    return this.http.get(environment.serverUrl + '/book');
  }

  public getPaging(pagingCounter: PagingModel): Observable<Object> {
    const body = { start: pagingCounter.start, step: pagingCounter.step };
    return this.http.post(environment.serverUrl + '/book/paging', body);
  }

  public getById(id: string): Observable<Object> {
    return this.http.get(environment.serverUrl + '/book/' + id);
  }

  public getByAuthorId(id: string): Observable<Object> {
    return this.http.get(environment.serverUrl + '/book/author/' + id);
  }

  public search(id: string, title: string, paging: any): Observable<Object> {
    let body = { 
      id: id, 
      title: title, 
      start: paging.start, 
      step: paging.step 
    };
    return this.http.post(environment.serverUrl + '/book/search', body);
  }

  public add(book: BookModel): Observable<Object> {
    return this.http.post(environment.serverUrl + '/book', book);
  }

  public update(book: BookModel, id: string): Observable<Object> {
    return this.http.put(environment.serverUrl + '/book/'+id, book);
  }

  public delete(id: string): Observable<Object> {
    return this.http.delete(environment.serverUrl + '/book/' + id);
  }

  public generateRandomText(quant: number, parag: number): String[] {
    let mainBlock: string[] = [];
    for (let i = 0; i < parag; i++) {
      let create = () => {
        let text: string = "Lorem";
        for (let i = 0; i < quant; i++) {
          text += " " + this.lipsumwords[Math.round(1 - 0.5 + Math.random() * ((100 - 1) - 0 + 1))] + " ";
        }
        return text;
      }
      mainBlock.push(create())
    }
    return mainBlock;
  }
}
