import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { IUserModel } from '../../../../../../shared/models/user.model';

@Injectable({
  providedIn: 'root'
})

export class AdminService {
  constructor(private http: HttpClient) { }

  public registration(user: IUserModel): Observable<Object> {
    const body = {
      userName: user.userName,
      email: user.email,
      password: user.password,
      conf: user.conf,
      role: user.role
    };
    return this.http.post(environment.serverUrl + '/admin', body);
  }

  public changeRole(id: string, role: number): Observable<Object> {
    const body = { id: id, role };
    return this.http.post(environment.serverUrl + '/admin/changerol', body);
  }

}
