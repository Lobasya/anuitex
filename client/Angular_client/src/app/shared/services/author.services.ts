import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { AuthorsModel } from '../models/authors.models';
import { PagingModel } from '../models/paging.models';

@Injectable()

export class AuthorServices {

  constructor(private http: HttpClient) { }

  public getAuthorById(id: string): Observable<Object> {
    return this.http.get(environment.serverUrl + '/author/' + id);
  }

  public getAll(): Observable<Object> {
    return this.http.get(environment.serverUrl + '/author');
  }

  public getAllPaginate(body: PagingModel): Observable<Object> {
    return this.http.post(environment.serverUrl + '/author/paging', body);
  }

  public search(id: string, title: string, paging: any): Observable<Object> {
    let body = {
      id: id,
      title: title,
      start: paging.start,
      step: paging.step
    };
    return this.http.post(environment.serverUrl + '/authors/search', body);
  }

  public add(body: AuthorsModel): Observable<Object> {
    return this.http.post(environment.serverUrl + '/author', body);
  }

  public delete(id: string): Observable<Object> {
    return this.http.delete(environment.serverUrl + '/author/' + id);
  }

  public update(author: AuthorsModel, id: string): Observable<Object> {
    return this.http.put(environment.serverUrl + '/author/'+id, author);
  }
}
