import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  constructor() { }

  public get<Type>(localKey: string): Type {
    return JSON.parse(localStorage.getItem(localKey))
  }

  public set(name: string, item: any): void {
    let saveditem = JSON.stringify(item)
    return localStorage.setItem(name, saveditem)
  }

  public remove(localKey: string): void {
    localStorage.setItem(localKey, null);
  }
}
