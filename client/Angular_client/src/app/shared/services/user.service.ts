import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { RegUser } from '../models/librery.models';
import { PagingModel } from '../models/paging.models';
import { IUserModel } from '../../../../../../shared/models/user.model';

@Injectable({
  providedIn: 'root'
})

export class UserServices {
  constructor(private http: HttpClient) { }


  public get(): Observable<Object> {
    return this.http.get(environment.serverUrl + '/admin');
  }

  public getPaging(pagingCounter: PagingModel): Observable<Object> {
    const body = {
      start: pagingCounter.start,
      step: pagingCounter.step
    };
    return this.http.post(environment.serverUrl + '/user/paging', body);
  }

  public search(id: string, title: string, paging: any): Observable<Object> {
    let body = {
      id: id,
      title: title,
      start: paging.start,
      step: paging.step
    };
    return this.http.post(environment.serverUrl + '/user/search', body);
  }

  public getById(id: string): Observable<Object> {
    return this.http.get(environment.serverUrl + '/user/' + id);
  }

  public update(id: string, user: RegUser): Observable<Object> {
    return this.http.put(environment.serverUrl + '/user/' + id, user);
  }

  public delete(id: string): Observable<Object> {
    return this.http.delete(environment.serverUrl + '/user/' + id);
  }

  public confirm(id: string, conf: boolean): Observable<Object> {
    const body = { id: id, conf: conf };
    return this.http.post(environment.serverUrl + '/admin/confirm', body);
  }

  public checkRole(): Observable<Object> {
    return this.http.get(environment.serverUrl + '/auth/user-role');
  }

  public edit(user: IUserModel): Observable<Object> {
    const body: IUserModel = {
      _id: user._id,
      userName: user.userName,
      email: user.email,
      password: user.password,
      conf: true
    };
    return this.http.post(environment.serverUrl + '/user', body);
  }

  public usersRoles() {
    return [{
      id: 1,
      title: "User"
    }, {
      id: 2,
      title: "Admin"
    }]
  }

  public pagesCounter(){
    return [4,5,6,10,15,20]
  }
}
