import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LocalStorageService } from '../services/local-storage.service';

@Injectable()
export class HeaderInterceptor implements HttpInterceptor {
    constructor( public localStorageService: LocalStorageService ) {}


    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!(req.url.indexOf('/auth/login') === -1)) {
            return next.handle(req);
        }
        if (!(req.url.indexOf('/auth/register') === -1)) {
            return next.handle(req);
        }
        const paramReq = req.clone({
            headers: new HttpHeaders ({'x-access-token' : this.localStorageService.get('user')['token']})
        });
        return next.handle(paramReq);
    }
}