export interface RegUser {
    _id?: string;
    userName: string;
    email: string;
    password: string;
    conf: boolean;
    role?: number;
}