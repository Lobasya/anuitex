export interface AuthorsModel {
    _id?: string,
    rexp?: string,
    name: string,
    img: String,
}
