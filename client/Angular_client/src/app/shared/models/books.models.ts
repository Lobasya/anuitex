export interface BookModel {
    title: String,
    rexp: String,
    img: any,
    authorId: string[],
    date: Date,
    length: Number,
    magazine: String[],
    _id?: String,
}