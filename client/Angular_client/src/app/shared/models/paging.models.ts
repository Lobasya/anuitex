export interface PagingModel {
    lengthArr: number,
    start: number,
    step: number,
    quantityBtn: number
}