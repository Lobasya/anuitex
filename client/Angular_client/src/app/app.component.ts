import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from './shared/services/local-storage.service';
import { UserServices } from './shared/services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'client';

  constructor(private rout: Router,
    private localStorageServices: LocalStorageService,
    private userServices: UserServices) {
    this.getUserRole();
  }

  public getUserRole(): void {
    if (this.localStorageServices.get('user') == null) {
      this.rout.navigate(['']);
      return
    }

    this.userServices.checkRole().subscribe(res => {
      let user = this.localStorageServices.get('user');
      user['user'].role = res;

      if (res == 1) {
        this.rout.navigate(['/user'])
        this.localStorageServices.set('user', user)
        return
      }
      this.rout.navigate(['/admin'])
      this.localStorageServices.set('user', user)
    })

  }
}
