import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StartPageComponent } from './component/start-page/start-page.component';
import { AdminGuardGuard } from './shared/guards/admin-guard.guard';
import { UserGuardGuard } from './shared/guards/user-guard.guard';

const routes: Routes = [
  {
    path: '', 
    component: StartPageComponent,
  }, 
  {
    path: "admin",
    canActivate: [AdminGuardGuard],
    loadChildren: "./component/admin/admin.module#AdminModule"
  },
  {
    path: "user",
    canActivate: [UserGuardGuard],
    loadChildren: "./component/user/user.module#UserModule"
  }, 
  {
    path: '**', 
    redirectTo: '' 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
