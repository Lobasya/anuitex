import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BooksAdminComponent } from './admin-books.component';

const routes: Routes = [
    { path: "", component: BooksAdminComponent},
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})
export class AdminBooksRoutingModule { }