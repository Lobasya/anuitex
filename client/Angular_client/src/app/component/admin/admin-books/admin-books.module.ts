import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AdminBooksRoutingModule } from './admin-books-routing.component';
import { BooksAdminComponent } from './admin-books.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgSelectModule } from '@ng-select/ng-select';
import { PreloaderModule } from '../../preloader/preloader.module';

@NgModule({
  imports: [
    AdminBooksRoutingModule, 
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgSelectModule,
    PreloaderModule
  ],
  declarations: [
    BooksAdminComponent,
  ]
})
export class AdminBooksModule {}