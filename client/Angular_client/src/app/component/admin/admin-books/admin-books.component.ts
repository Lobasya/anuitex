import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { PagingModel } from 'src/app/shared/models/paging.models';
import { BookModel } from '../../../shared/models/books.models';
import { BookServices } from '../../../shared/services/book.services';
import { Router } from '@angular/router';

@Component({
  selector: 'books-admin',
  templateUrl: './admin-books.component.html',
  styleUrls: ['./admin-books.component.scss']
})
export class BooksAdminComponent implements OnInit {
  public filterallBooksArr: BookModel[];
  public searchInputValue: string;
  public timer = null;
  public isLoader = false;
  public pagingCounter: PagingModel = {
    lengthArr: 0,
    start: 0,
    step: 4,
    quantityBtn: 0
  }

  constructor(private bookService: BookServices,
    private local: LocalStorageService,
    private router: Router) {
  }

  ngOnInit() {
    this.getPaging();
   }

  public getPaging(): void {
    this.isLoader = true;
    this.bookService.get().subscribe((res: object[]) => {
      this.pagingCounter.lengthArr = res.length;
    })
    this.bookService.getPaging(this.pagingCounter).subscribe((res: BookModel[]) => {
      this.filterallBooksArr = res;
      this.isLoader = false;
    })
  }

  public search(): void {
    if(this.timer !== null){
      clearTimeout(this.timer);
    }
    let id: string = this.local.get('user')['user']._id;
    this.filterallBooksArr = [];
    this.timer = setTimeout(() => {
      if(this.searchInputValue.length <= 0){
        this.getPaging();
      }
      this.bookService.search(id, this.searchInputValue, this.pagingCounter).subscribe((books: any) => {
        console.log(books)
        this.pagingCounter.lengthArr = books.booksLength;
        this.filterallBooksArr = books.booksArr;
      })
    }, 500)
  }

  public getPaginationValue(p): void {
    this.isLoader = true;
    this.pagingCounter.start = p-1;
    this.bookService.getPaging(this.pagingCounter).subscribe((res: BookModel[]) => {
      this.filterallBooksArr = res;
      this.isLoader = false;
    })
  }

  public delete(event): void {
    let id: string = event.target.id;
    this.bookService.delete(id).subscribe(res => {
      this.getPaging();
    })
  }

}
