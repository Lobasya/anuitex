import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MagazineModel } from 'src/app/shared/models/magazine.models';
import { BookModel } from '../../../shared/models/books.models';
import { AuthorsModel } from '../../../shared/models/authors.models';
import { UserServices } from 'src/app/shared/services/user.service';
import { BookServices } from '../../../shared/services/book.services';
import { AuthorServices } from 'src/app/shared/services/author.services';
import { MagazineServices } from 'src/app/shared/services/magazine.services';

@Component({
  selector: 'add-book',
  templateUrl: './admin-add-book.component.html',
  styleUrls: ['./admin-add-book.component.scss']
})
export class AdminAddBookComponent implements OnInit {
  public allAuthorsArr: AuthorsModel[];
  public allMagazineArr: MagazineModel[];

  public selectedAuthorId: string[];
  public selectedMagazineId: string[];


  public imgBase: string;
  public isImageOnLoadState: boolean = false;

  public addBookFormGroup: FormGroup = new FormGroup({
    title: new FormControl(),
    date: new FormControl(),
    length: new FormControl(),
    magazineId: new FormControl(),
    img: new FormControl(),
    author: new FormControl()
  })
  constructor(private userServices: UserServices,
    private bookService: BookServices,
    private authorServices: AuthorServices,
    private magazineServices: MagazineServices){
  }

  ngOnInit() {
    this.getAll();
    this.getMagazine()
  }

  public add(): void {
    let book: BookModel = {
      title: this.addBookFormGroup.value.title,
      rexp: this.addBookFormGroup.value.title.toLowerCase(),
      authorId: this.selectedAuthorId,
      img: this.imgBase,
      date: this.addBookFormGroup.value.date,
      length: this.addBookFormGroup.value.length,
      magazine: this.selectedMagazineId
    }
    this.bookService.add(book).subscribe(res => {
    })
  }

  public getAll(): void {
    this.authorServices.getAll().subscribe((res: AuthorsModel[]) => {
      this.allAuthorsArr = res;
    })
  }

  public getMagazine(): void {
    this.magazineServices.get().subscribe((res: MagazineModel[]) => {
      this.allMagazineArr = res;
    })
  }

  public imgToBaseBook(event) {
    var file = event.target.files[0];
    const saveImg = (img: string) => {
      this.imgBase = img;
      this.isImageOnLoadState = true;
    };
    var reader = new FileReader();
    reader.onloadend = function () {
      saveImg(reader.result.toString())
    }
    reader.readAsDataURL(file);
  }

}
