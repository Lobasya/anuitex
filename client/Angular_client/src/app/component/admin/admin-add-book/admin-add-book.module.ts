import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AdminAddBookRoutingModule } from './admin-add-book-routing.component';
import { AdminAddBookComponent } from './admin-add-book.component';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    AdminAddBookRoutingModule, 
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule
  ],
  declarations: [
    AdminAddBookComponent
  ]
})
export class AdminAddBookModule {}