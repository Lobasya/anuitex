
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminAddBookComponent } from './admin-add-book.component';

const routes: Routes = [
    { path: "", component: AdminAddBookComponent},
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})
export class AdminAddBookRoutingModule { }