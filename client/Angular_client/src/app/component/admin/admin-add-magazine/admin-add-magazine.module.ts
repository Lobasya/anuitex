import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { AdminAddMagazineComponent } from './admin-add-magazine.component';
import { AdminAddMagazineRoutingModule } from './admin-add-magazine-routing.component';

@NgModule({
  imports: [
    AdminAddMagazineRoutingModule, 
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule
  ],
  declarations: [
    AdminAddMagazineComponent
  ]
})
export class AdminAddMagazineModule {}