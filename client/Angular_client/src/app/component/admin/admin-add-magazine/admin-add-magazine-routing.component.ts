import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminAddMagazineComponent } from './admin-add-magazine.component';

const routes: Routes = [
    { path: "", component: AdminAddMagazineComponent},

];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})
export class AdminAddMagazineRoutingModule { }