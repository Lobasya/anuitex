import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MagazineModel } from 'src/app/shared/models/magazine.models';
import { MagazineServices } from 'src/app/shared/services/magazine.services';



@Component({
  selector: 'admin-add-magazine',
  templateUrl: './admin-add-magazine.component.html',
  styleUrls: ['./admin-add-magazine.component.scss']
})
export class AdminAddMagazineComponent implements OnInit {
  public imgBase: string;
  public isImageOnLoadState: boolean = false;

  public addMagazineFormGroup: FormGroup = new FormGroup({
    title: new FormControl(),
    img: new FormControl(),
  })

  constructor(private magazineServices: MagazineServices) { }

  ngOnInit() {
  }

  public add(): void {
    let body: MagazineModel = {
      title: this.addMagazineFormGroup.value.title,
      img: this.imgBase
    }
    this.magazineServices.add(body).subscribe(res => {
      //asdsas
    })
  }

  public imgToBaseBook(event) {
    var file = event.target.files[0];
    const saveImg = (img: string) => {
      this.imgBase = img;
    };
    var reader = new FileReader();
    reader.onloadend = function () {
      saveImg(reader.result.toString())
    }
    reader.readAsDataURL(file);
  }
}
