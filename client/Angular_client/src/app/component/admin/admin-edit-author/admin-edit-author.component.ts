import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookServices } from 'src/app/shared/services/book.services';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthorServices } from 'src/app/shared/services/author.services';
import { AuthorsModel } from 'src/app/shared/models/authors.models';

@Component({
  selector: 'admin-edit-author',
  templateUrl: './admin-edit-author.component.html',
  styleUrls: ['./admin-edit-author.component.scss']
})
export class AdminEditAuthorComponent implements OnInit {
  public idPage: string;

  public imgBase: string;

  public author: AuthorsModel = {
    name: '',
    img: '',
  };

  public editAuthorForm: FormGroup = new FormGroup({
    name: new FormControl(),
    img: new FormControl(),
    descr: new FormControl()
  })

  constructor(private route: ActivatedRoute,
    private authorServices: AuthorServices) {
  }

  ngOnInit() {
    this.getPageId();
    this.getAuthor();
  }

  public getPageId(): void {
    this.route.params.subscribe(params => {
      this.idPage = params['authorId'];
    });
  }

  public getAuthor(): void {
    this.authorServices.getAuthorById(this.idPage).subscribe((author: AuthorsModel) => {
      if (!author.img) {
        this.author = author;
        this.author.img = "http://www.afics-kenya.org/assets/frontend/pages/img/people/img1-large.png";
        return
      }
      this.author = author;
    })
  }

  public imgToBaseBook(event): void {
    var file = event.target.files[0];
    const saveImg = (img: string) => {
      this.author.img = img;
    };
    var reader = new FileReader();
    reader.onloadend = function () {
      saveImg(reader.result.toString())
    }
    reader.readAsDataURL(file);
  }

  public sendUpdateAuthor(): void {
    this.authorServices.update(this.author, this.idPage).subscribe((author: AuthorsModel) => {
      this.getAuthor();
    })
  }

}
