import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookModel } from 'src/app/shared/models/books.models';
import { BookServices } from 'src/app/shared/services/book.services';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthorServices } from 'src/app/shared/services/author.services';
import { AuthorsModel } from 'src/app/shared/models/authors.models';
import { MagazineModel } from 'src/app/shared/models/magazine.models';
import { MagazineServices } from 'src/app/shared/services/magazine.services';

@Component({
  selector: 'admin-edit-book',
  templateUrl: './admin-edit-book.component.html',
  styleUrls: ['./admin-edit-book.component.scss']
})
export class AdminEditBookComponent implements OnInit {
  public idPage: string;
  public allAuthorsArr: AuthorsModel[];
  public allMagazinesArr: MagazineModel[];

  public imgBase: string;

  public book: BookModel = {
    title: '',
    rexp: '',
    img: '',
    authorId: [],
    date: new Date(),
    length: 123,
    magazine: []
  };

  public editBookForm: FormGroup = new FormGroup({
    title: new FormControl(),
    authorId: new FormControl(),
    magazines: new FormControl(),
    img: new FormControl(),
    date: new FormControl(),
    length: new FormControl(),
    descr: new FormControl()
  })

  constructor(private route: ActivatedRoute,
    private bookServices: BookServices,
    private authorServices: AuthorServices,
    private magazineServices: MagazineServices) {
  }

  ngOnInit() {
    this.getPageId();
    this.getBook();
    this.getAllAuthors();
    this.getAllMagazine();
  }

  public getPageId(): void {
    this.route.params.subscribe(params => {
      this.idPage = params['bookId'];
    });
  }

  public getBook(): void {
    this.bookServices.getById(this.idPage).subscribe((book: BookModel) => {
      this.book = book[0];
      this.book.authorId = [].map.call(this.book.authorId, item =>{
        return item._id;
      })
    })
  }

  public getAllAuthors(): void {
    this.authorServices.getAll().subscribe((authors: AuthorsModel[]) => {
      this.allAuthorsArr = authors;
    })
  }

  public getAllMagazine(): void {
    this.magazineServices.get().subscribe((magazines: MagazineModel[]) => {
      this.allMagazinesArr = magazines;
    })
  }

  public imgToBaseBook(event): void {
    var file = event.target.files[0];
    const saveImg = (img: string) => {
      this.book.img = img;
    };
    var reader = new FileReader();
    reader.onloadend = function () {
      saveImg(reader.result.toString())
    }
    reader.readAsDataURL(file);
  }

  public sendUpdateBook(): void {
    this.bookServices.update(this.book, this.idPage).subscribe((book: BookModel) => {
      this.getBook();
    })
  }

}
