import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AdminGuardGuard } from 'src/app/shared/guards/admin-guard.guard';
import { UsersAdminComponent } from './admin-users/admin-users.component';
import { AdminEditBookComponent } from './admin-edit-book/admin-edit-book.component';
import { AdminEditAuthorComponent } from './admin-edit-author/admin-edit-author.component';
import { AdminEditMagazineComponent } from './admin-edit-magazine/admin-edit-magazine.component';
import { AdminEditUserComponent } from './admin-edit-user/admin-edit-user.component';

const routes: Routes = [
  {
    path: '', 
    canActivate: [AdminGuardGuard],
    component: AdminComponent,
    children: [
      {
        path: "",
        component: UsersAdminComponent
      },
      {
        path: "edit-book/:bookId",
        component: AdminEditBookComponent
      },
      {
        path: "edit-author/:authorId",
        component: AdminEditAuthorComponent
      },
      {
        path: "edit-magazine/:magazineId",
        component: AdminEditMagazineComponent
      },
      {
        path: "edit-user/:userId",
        component: AdminEditUserComponent
      },
      {
        path: "books",
        loadChildren: "./admin-books/admin-books.module#AdminBooksModule"
      },
      {
        path: "add-user",
        loadChildren: "./admin-add-user/admin-add.module#AdminAddModule"
      },
      {
        path: "add-book",
        loadChildren: "./admin-add-book/admin-add-book.module#AdminAddBookModule"
      },
      {
        path: "add-author",
        loadChildren: "./admin-add-author/admin-add-author.module#AdminAddAuthorModule"
      },
      {
        path: "add-magazine",
        loadChildren: "./admin-add-magazine/admin-add-magazine.module#AdminAddMagazineModule"
      },
      {
        path: "authors",
        loadChildren: "./admin-authors/admin-authors.module#AdminAuthorsModule"
      },
      {
        path: "magazines",
        loadChildren: "./admin-magazines/admin-magazines.module#AdminMagazinesModule"
      },
    ]
},
  {path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
