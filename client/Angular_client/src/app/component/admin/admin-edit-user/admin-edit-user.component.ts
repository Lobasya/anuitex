import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookServices } from 'src/app/shared/services/book.services';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthorServices } from 'src/app/shared/services/author.services';
import { RegUser } from 'src/app/shared/models/librery.models';
import { UserServices } from 'src/app/shared/services/user.service';

@Component({
  selector: 'admin-edit-user',
  templateUrl: './admin-edit-user.component.html',
  styleUrls: ['./admin-edit-user.component.scss']
})
export class AdminEditUserComponent implements OnInit {
  public idPage: string;
  public userConfirmStatus: string = '';
  public imageUser: String = 'https://png.pngtree.com/svg/20170809/user_1045729.png';

  public roleArr = this.userServices.usersRoles();

  public user: RegUser = {
    userName: '',
    email: '',
    password: '',
    conf: true,
    role: 1
  };

  public editUserForm: FormGroup = new FormGroup({
    userName: new FormControl(),
    email: new FormControl(),
    descr: new FormControl(),
    role: new FormControl()
  })

  constructor(private route: ActivatedRoute,
    private bookServices: BookServices,
    private authorServices: AuthorServices,
    private userServices: UserServices) {
  }

  ngOnInit() {
    this.getPageId();
    this.getUser();
  }

  public getPageId(): void {
    this.route.params.subscribe(params => {
      this.idPage = params['userId'];
    });
  }

  public getUser(): void {
    this.userServices.getById(this.idPage).subscribe((user: RegUser) => {
      this.user = user;
      if (this.user.conf) {
        this.userConfirmStatus = 'User confirmed';
        return;
      }
      this.userConfirmStatus = 'User unconfirmed';
    })
  }

  public changeConfirmUser(): void {
    this.user.conf = !this.user.conf;
  }


  public sendUpdateUser(): void {
    if (this.user.role == null) {
      this.user.role = 1;
    }
    this.userServices.update(this.idPage, this.user).subscribe((user: RegUser) => {
      this.getUser();
    })
  }

}
