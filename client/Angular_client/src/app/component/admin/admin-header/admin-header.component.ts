import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';


@Component({
  selector: 'admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss']
})

export class HeaderComponent implements OnInit {
  public userName: string = this.localStorageServices.get('user')['user'].userName;
  public isAuth: boolean = false;

  constructor(private localStorageServices: LocalStorageService,
  private rout: Router) { }

  ngOnInit() { }

  public logOut(): void {
    this.localStorageServices.remove('user');
    this.rout.navigate([''])
  }

}
