import { Component, OnInit } from '@angular/core';
import { PagingModel } from 'src/app/shared/models/paging.models';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { MagazineServices } from 'src/app/shared/services/magazine.services';
import { MagazineModel } from 'src/app/shared/models/magazine.models';



@Component({
  selector: 'admin-magazines',
  templateUrl: './admin-magazines.component.html',
  styleUrls: ['./admin-magazines.component.scss']
})

export class AdminMagazinesComponent implements OnInit {
  public filterAllMagazinesArr: MagazineModel[];
  public searchInputValue: string;
  public timer = null;
  public isLoader = false;

  public pagingCounter: PagingModel = {
    lengthArr: 0,
    start: 0,
    step: 4,
    quantityBtn: 0
  }
  constructor(private magazineServices: MagazineServices,
    private localStorageService: LocalStorageService ) {
  }

  ngOnInit() {
    this.getPaging();
  }

  public getPaging(): void {
    this.isLoader = true;
    this.magazineServices.get().subscribe((res: object[]) => {
      this.pagingCounter.lengthArr = res.length;
    })
    this.magazineServices.getPaginate(this.pagingCounter).subscribe((res: MagazineModel[]) => {
      res.map(item => {
        if (!item.img) {
          return item.img = "http://www.afics-kenya.org/assets/frontend/pages/img/people/img1-large.png";
        }
      })
      this.filterAllMagazinesArr = res;
      this.isLoader = false;
    })
  }

  public search(): void {
    if(this.timer !== null){
      clearTimeout(this.timer);
    }
    let id: string = this.localStorageService.get('user')['user']._id;
    this.filterAllMagazinesArr = [];
    this.timer = setTimeout(() => {
      if (this.searchInputValue.length <= 0) {
        this.getPaging();
      }
      this.magazineServices.search(id, this.searchInputValue, this.pagingCounter).subscribe((magazine: any) => {
        console.log(magazine)
        this.pagingCounter.lengthArr = magazine.magazinesLength;
        magazine.magazinesArr.map(item => {
          if (!item.img) {
            return item.img = "http://www.afics-kenya.org/assets/frontend/pages/img/people/img1-large.png";
          }
        })
        this.filterAllMagazinesArr = magazine.magazinesArr;
      })
    }, 1000)
  }

  public getPaginationValue(p): void {
    this.isLoader = true;
    this.pagingCounter.start = p - 1;
    this.magazineServices.getPaginate(this.pagingCounter).subscribe((res: MagazineModel[]) => {
      res.map(item => {
        if (!item.img) {
          return item.img = "http://www.afics-kenya.org/assets/frontend/pages/img/people/img1-large.png";
        }
      })
      this.filterAllMagazinesArr = res;
      this.isLoader = false;
    })
  }

  public delete(event): void {
    let id: string = event.target.id;
    this.magazineServices.delete(id).subscribe(res => {
      this.getPaging();
    })
  }

}
