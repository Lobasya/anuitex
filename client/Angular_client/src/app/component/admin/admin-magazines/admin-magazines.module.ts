import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { AdminMagazinesRouting } from './admin-magazines-routing.component';
import { AdminMagazinesComponent } from './admin-magazines.component';
import { PreloaderModule } from '../../preloader/preloader.module';

@NgModule({
  imports: [
    AdminMagazinesRouting, 
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    PreloaderModule
  ],
  declarations: [
    AdminMagazinesComponent,
  ]
})
export class AdminMagazinesModule {}