import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})

export class AdminComponent implements OnInit {
  public userName: string = this.localStorageService.get('user')['user'].userName;
  public isAuth: boolean = false;
  public isUsersComp: boolean = true;
  public isAddComp: boolean = false;
  public isBooksComp: boolean = false;

  constructor(private localStorageService: LocalStorageService, 
  private rout: Router) { }

  ngOnInit() { }

  public logOut(): void {
    localStorage.setItem('user', null);
    this.rout.navigate([''])
  }


}
