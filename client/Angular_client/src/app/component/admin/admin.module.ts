import { NgModule } from "@angular/core";
import { AdminRoutingModule } from './admin-routing.module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminComponent } from './admin.component';
import { HeaderComponent } from './admin-header/admin-header.component';
import { UsersAdminComponent } from './admin-users/admin-users.component';
import { AdminEditBookComponent } from './admin-edit-book/admin-edit-book.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { AdminEditAuthorComponent } from './admin-edit-author/admin-edit-author.component';
import { AdminEditMagazineComponent } from './admin-edit-magazine/admin-edit-magazine.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { AdminEditUserComponent } from './admin-edit-user/admin-edit-user.component';
import { PreloaderModule } from '../preloader/preloader.module';

@NgModule({
  imports: [
    AdminRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgxPaginationModule,
    PreloaderModule
  ],
  declarations: [
    AdminComponent,
    HeaderComponent,
    UsersAdminComponent,
    AdminEditBookComponent,
    AdminEditAuthorComponent,
    AdminEditMagazineComponent,
    AdminEditUserComponent,
  ]
})
export class AdminModule { }