import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { AdminAuthorsRouting } from './admin-authors-routing.component';
import { AdminAuthorsComponent } from './admin-authors.component';
import { PreloaderModule } from '../../preloader/preloader.module';

@NgModule({
  imports: [
    AdminAuthorsRouting, 
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    PreloaderModule
  ],
  declarations: [
    AdminAuthorsComponent,
  ]
})
export class AdminAuthorsModule {}