import { Component, OnInit } from '@angular/core';
import { PagingModel } from 'src/app/shared/models/paging.models';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { BookModel } from 'src/app/shared/models/books.models';
import { AuthorServices } from 'src/app/shared/services/author.services';



@Component({
  selector: 'admin-authors',
  templateUrl: './admin-authors.component.html',
  styleUrls: ['./admin-authors.component.scss']
})

export class AdminAuthorsComponent implements OnInit {
  public filterAllAuthorsArr: BookModel[];
  public searchInputValue: string;
  public timer = null;
  public isLoader = false;
  public pagingCounter: PagingModel = {
    lengthArr: 0,
    start: 0,
    step: 4,
    quantityBtn: 0
  }
  constructor(private authorServices: AuthorServices,
    private localStorageService: LocalStorageService, ) {
  }

  ngOnInit() {
    this.getPaging();
  }

  public getPaging(): void {
    this.isLoader = true;
    this.authorServices.getAll().subscribe((res: object[]) => {
      this.pagingCounter.lengthArr = res.length;
    })
    this.authorServices.getAllPaginate(this.pagingCounter).subscribe((res: BookModel[]) => {
      res.map(item => {
        if (!item.img) {
          return item.img = "http://www.afics-kenya.org/assets/frontend/pages/img/people/img1-large.png";
        }
      })
      this.filterAllAuthorsArr = res;
      this.isLoader = false;
    })
  }

  public search(): void {
    if (this.timer !== null) {
      clearTimeout(this.timer);
    }
    let id: string = this.localStorageService.get('user')['user']._id;
    this.filterAllAuthorsArr = [];
    this.timer = setTimeout(() => {
      if (this.searchInputValue.length <= 0) {
        this.getPaging();
      }
      this.authorServices.search(id, this.searchInputValue, this.pagingCounter).subscribe((authors: any) => {
        this.pagingCounter.lengthArr = authors.authorsLength;
        authors.authorsArr.map(item => {
          if (!item.img) {
            return item.img = "http://www.afics-kenya.org/assets/frontend/pages/img/people/img1-large.png";
          }
        })
        this.filterAllAuthorsArr = authors.authorsArr;
      })
    }, 500)
  }

  public getPaginationValue(p): void {
    this.isLoader = true;
    this.pagingCounter.start = p - 1;
    this.authorServices.getAllPaginate(this.pagingCounter).subscribe((res: BookModel[]) => {
      res.map(item => {
        if (!item.img) {
          return item.img = "http://www.afics-kenya.org/assets/frontend/pages/img/people/img1-large.png";
        }
      })
      this.filterAllAuthorsArr = res;
      this.isLoader = false;
    })
  }

  public delete(event): void {
    let id: string = event.target.id;
    this.authorServices.delete(id).subscribe(res => {
      this.getPaging();
    })
  }

}
