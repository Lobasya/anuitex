import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { AdminAddAuthorComponent } from './admin-add-author.component';
import { AdminAddAuthorRoutingModule } from './admin-add-author-routing.component';

@NgModule({
  imports: [
    AdminAddAuthorRoutingModule, 
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule
  ],
  declarations: [
    AdminAddAuthorComponent
  ]
})
export class AdminAddAuthorModule {}