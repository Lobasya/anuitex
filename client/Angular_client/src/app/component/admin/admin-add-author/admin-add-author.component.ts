import { Component, OnInit } from '@angular/core';
import { AuthorServices } from 'src/app/shared/services/author.services';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'admin-add-author',
  templateUrl: './admin-add-author.component.html',
  styleUrls: ['./admin-add-author.component.scss']
})
export class AdminAddAuthorComponent implements OnInit {
  public imgBaseAuthor: string;
  public isImageOnLoadState: boolean = false;
  
  public addAuthorFormGroup: FormGroup = new FormGroup({
    authorName: new FormControl(),
    authorSurname: new FormControl(),
    img: new FormControl()
  })
  constructor(private authorServices: AuthorServices) { }

  ngOnInit() {
  }

  public add(): void {
    let name: string = this.addAuthorFormGroup.value.authorSurname + ' ' + this.addAuthorFormGroup.value.authorName;
    this.authorServices.add({ name: name, rexp: name.toLowerCase(), img: this.imgBaseAuthor }).subscribe(res => {
    })
  }

  public imgToBaseAuthor(event) {
    var file = event.target.files[0];
    const saveImg = (img: string) => {
      this.imgBaseAuthor = img;
      this.isImageOnLoadState = true;
    };
    var reader = new FileReader();
    reader.onloadend = function () {
      saveImg(reader.result.toString())
    }
    reader.readAsDataURL(file);
  }
  

}
