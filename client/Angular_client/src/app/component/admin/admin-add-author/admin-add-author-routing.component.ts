
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminAddAuthorComponent } from './admin-add-author.component';

const routes: Routes = [
    { path: "", component: AdminAddAuthorComponent},

];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})
export class AdminAddAuthorRoutingModule { }