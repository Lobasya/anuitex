import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { AdminAddRoutingModule } from './admin-add-routing.component';
import { AddAdminComponent } from './admin-add.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    AdminAddRoutingModule, 
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    AddAdminComponent
  ]
})
export class AdminAddModule {}