
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddAdminComponent } from './admin-add.component';

const routes: Routes = [
    { path: "", component: AddAdminComponent},

];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})
export class AdminAddRoutingModule { }