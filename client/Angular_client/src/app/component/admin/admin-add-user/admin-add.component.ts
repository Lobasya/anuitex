import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AdminService } from '../../../shared/services/admin.services';
import { Router } from '@angular/router';


@Component({
  selector: 'admin-add-user',
  templateUrl: './admin-add.component.html',
  styleUrls: ['./admin-add.component.scss']
})

export class AddAdminComponent implements OnInit {
  public statusReg: string;

  public addNewUser: FormGroup = new FormGroup({
    userName: new FormControl(),
    email: new FormControl(),
    password: new FormControl(),
    conf: new FormControl(true),
    role: new FormControl()
  })

  constructor(private adminRegServices: AdminService,
  private router: Router) { }

  ngOnInit() {}
  
  public submitFormReg(): void {
    this.addNewUser.value.role = (!this.addNewUser.value.role) ? 1 : +this.addNewUser.value.role;
    this.adminRegServices.registration(this.addNewUser.value).subscribe(res => {
      this.statusReg = 'User created';
      this.router.navigate(['/admin/users'])
    },
    err =>{
      this.statusReg = 'Error! User are not created';
    })

  }

}
