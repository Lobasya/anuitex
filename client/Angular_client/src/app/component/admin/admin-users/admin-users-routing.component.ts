
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { UsersAdminComponent } from './admin-users.component';



const routes: Routes = [
    { path: "", component: UsersAdminComponent},

];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})
export class AdminUsersRoutingModule { }