import { Component, OnInit } from '@angular/core';
import { AdminService } from 'src/app/shared/services/admin.services';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { RegUser } from 'src/app/shared/models/librery.models';
import { UserServices } from 'src/app/shared/services/user.service';
import { PagingModel } from 'src/app/shared/models/paging.models';
import { IUserModel } from '../../../../../../../shared/models/user.model';


@Component({
  selector: 'admin-users-admin',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.scss']
})

export class UsersAdminComponent implements OnInit {
  public allUsersArr: IUserModel[];
  public userId: string;
  public isLoader: Boolean = false;
  public searchInputValue: string;

  public pagingCounter: PagingModel = {
    lengthArr: 0,
    start: 0,
    step: 4,
    quantityBtn: 0
  }

  constructor(private LocalServices: LocalStorageService,
    private adminServices: AdminService,
    private userServices: UserServices,
    private localStorageServices: LocalStorageService,
    private rout: Router) {
    this.getPaging();
  }

  ngOnInit() { }

  public getAllUsers(): void {
    this.userServices.get().subscribe(
      (res: IUserModel[]) => {
        this.allUsersArr = res;
      })
  }
  
  public getPaginationValue(p): void {
    this.isLoader = true;
    this.pagingCounter.start = p-1;
    this.userServices.getPaging(this.pagingCounter).subscribe((res: IUserModel[]) => {
      this.allUsersArr = res;
      this.isLoader = false;
    })
  }

  public getPaging(): void {
    this.isLoader = true;
    this.userServices.get().subscribe((res: object[]) => {
      this.pagingCounter.lengthArr = res.length;
    })
    
    this.userServices.getPaging(this.pagingCounter).subscribe((res: IUserModel[]) => {
      this.allUsersArr = res;
      this.isLoader = false;
    })
  }

  public confirm(event): void {
    this.userId = this.allUsersArr[event.target.id]._id;
    const userConf = this.allUsersArr[event.target.id].conf;
    this.userServices.confirm(this.userId, userConf).subscribe(res => {
      this.getAllUsers();
    })
  }

  public search(): void {
    let id: string = this.localStorageServices.get('user')['user']._id;
    this.allUsersArr = [];
    setTimeout(() => {
      if(this.searchInputValue.length <= 0){
        this.getPaging();
      }
      this.userServices.search(id, this.searchInputValue, this.pagingCounter).subscribe((users: any) => {
        this.pagingCounter.lengthArr = users.booksLength;
        this.allUsersArr = users.booksArr;
      })
    }, 500)
  }
 
  public delete(event): void {
    this.userId = this.allUsersArr[event.target.id]._id;
    if (this.LocalServices.get('user')['user']._id == this.userId) {
      return;
    }
    this.userServices.delete(this.userId).subscribe(res => {
      this.getAllUsers();
    })
  }

}
