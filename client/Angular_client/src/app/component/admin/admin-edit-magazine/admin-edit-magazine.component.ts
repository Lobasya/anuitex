import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookServices } from 'src/app/shared/services/book.services';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthorServices } from 'src/app/shared/services/author.services';
import { MagazineModel } from 'src/app/shared/models/magazine.models';
import { MagazineServices } from 'src/app/shared/services/magazine.services';

@Component({
  selector: 'admin-edit-magazine',
  templateUrl: './admin-edit-magazine.component.html',
  styleUrls: ['./admin-edit-magazine.component.scss']
})
export class AdminEditMagazineComponent implements OnInit {
  public idPage: string;

  public imgBase: string;

  public magazine: MagazineModel = {
    title: '',
    img: '',
  };

  public editMagazineForm: FormGroup = new FormGroup({
    title: new FormControl(),
    img: new FormControl(),
    descr: new FormControl()
  })

  constructor(private route: ActivatedRoute,
    private bookServices: BookServices,
    private magazineServices: MagazineServices,
    private authorServices: AuthorServices) {
  }

  ngOnInit() {
    this.getPageId();
    this.getMagazine();
  }

  public getPageId(): void {
    this.route.params.subscribe(params => {
      this.idPage = params['magazineId'];
    });
  }

  public getMagazine(): void {

    this.magazineServices.getById(this.idPage).subscribe((magazine: MagazineModel) => {
      console.log(this.idPage, magazine)
      if (!magazine.img && magazine) {
        this.magazine = magazine;
        this.magazine.img = "http://www.afics-kenya.org/assets/frontend/pages/img/people/img1-large.png";
        return
      }
      this.magazine = magazine;
      console.log(magazine)
    })
  }

  public imgToBaseBook(event) {
    var file = event.target.files[0];
    const saveImg = (img: string) => {
      this.magazine.img = img;
    };
    var reader = new FileReader();
    reader.onloadend = function () {
      saveImg(reader.result.toString())
    }
    reader.readAsDataURL(file);
  }

  public sendUpdateMagazine(): void {
    this.magazineServices.update(this.magazine, this.idPage).subscribe((magazine: MagazineModel) => {
      this.magazine = magazine;
    })
  }

}
