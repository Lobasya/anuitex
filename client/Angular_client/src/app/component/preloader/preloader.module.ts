import { NgModule } from "@angular/core";
import { PreloaderComponent } from './preloader.component';
import { PreloaderRoutingModule } from './preloader-routing.module';

@NgModule({
  imports: [
    PreloaderRoutingModule,
  ],
  entryComponents:[
    PreloaderComponent
  ],
  exports:[
    PreloaderComponent
  ],
  declarations: [
    PreloaderComponent
  ]
})
export class PreloaderModule { }