import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { AuthServices } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.scss']
})
export class StartPageComponent implements OnInit {
  public isRegState: boolean = false;
  public typeOfForm: string = 'register';
  public authStatus: string;

  public regFormGroup: FormGroup = new FormGroup({
    userName: new FormControl(),
    email: new FormControl(),
    password: new FormControl(),
    conf: new FormControl(false),
  })

  public loginFormGroup: FormGroup = new FormGroup({
    email: new FormControl(),
    password: new FormControl(),
  })

  constructor(private authServices: AuthServices,
    private localStorageService: LocalStorageService,
    private rout: Router) { }

  ngOnInit() { }

  public setRegState(event): void {
    this.isRegState = !this.isRegState;
    if (event.target.name === 'login') {
      this.isRegState = false;
      this.typeOfForm = "register";
      return;
    }
    this.isRegState = true;
    this.typeOfForm = "login";
  }

  public submitFormReg(): void {
    this.authServices.registration(this.regFormGroup.value).subscribe(res => {
      this.isRegState = false;
      this.typeOfForm = 'login';
    })
  }

  public submitFormLogin(): void {
    this.authServices.login(this.loginFormGroup.value).subscribe(
      res => {
        this.localStorageService.set('user', res)
        let role = this.localStorageService.get('user')['user'].role;
        if (!res['user']['conf']) {
          this.authStatus = 'Your profile are not confirmed';
          return
        }
        (role == 1) ? this.rout.navigate(['/user']) : this.rout.navigate(['/admin'])
      },
      err => (err.status === 400) ? this.authStatus = "No user found" : this.authStatus = "Password is not correct"
    )
  }

};
