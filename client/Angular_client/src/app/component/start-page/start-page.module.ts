import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { StartPageComponent } from './start-page.component';
import { StartPageRoutingModule } from './start-page-routing.module';

@NgModule({
  imports: [StartPageRoutingModule,CommonModule],
  declarations: [StartPageComponent]
})
export class UserModule {}