import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { UserAuthorComponent } from './user-author.component';
import { UserAuthorRoutingModule } from './user-author-routing.module';


@NgModule({
  imports: [
    UserAuthorRoutingModule, 
    CommonModule,
  ],
  declarations: [
    UserAuthorComponent
  ]
})
export class UserAuthorModule {}