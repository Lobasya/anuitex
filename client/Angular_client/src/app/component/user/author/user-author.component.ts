import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookModel } from 'src/app/shared/models/books.models';
import { AuthorsModel } from 'src/app/shared/models/authors.models';
import { AuthorServices } from 'src/app/shared/services/author.services';
import { BookServices } from 'src/app/shared/services/book.services';

@Component({
  selector: 'user-author',
  templateUrl: './user-author.component.html',
  styleUrls: ['./user-author.component.scss']
})
export class UserAuthorComponent implements OnInit {
  public idAuthor: string;
  public author: AuthorsModel = {
    _id: '',
    rexp: '',
    name: '',
    img: 'http://www.afics-kenya.org/assets/frontend/pages/img/people/img1-large.png',
  }
  public books: BookModel[];
  
  constructor(private route: ActivatedRoute,
    private authorServices: AuthorServices,
    private bookServices: BookServices) {
  }

  ngOnInit() { 
    this.getId();
    this.getById();
    this.getByAuthorId();
  }

  public getId(): void {
    this.route.params.subscribe(params => {
      this.idAuthor = params['authorId'];
    });
  }

  public getById(): void {
    this.authorServices.getAuthorById(this.idAuthor)
      .subscribe((res: AuthorsModel) => {
          if(!res.img){
            return res.img = "http://www.afics-kenya.org/assets/frontend/pages/img/people/img1-large.png";
          }
        this.author = res;
      })
  }

  public getByAuthorId() {
    this.bookServices.getByAuthorId(this.idAuthor)
      .subscribe((res: BookModel[]) => {
        this.books = res;
      })
  }

}
