import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';;
import { UserComponent } from './user.component';
import { UserSearchComponent } from './search/search.component';
import { UserBookComponent } from './user-book/user-book.component';
import { UserAuthorComponent } from './author/user-author.component';

const routes: Routes = [
  {
    path: '', 
    component: UserComponent,
    children: [
      {
        path: "",
        component: UserSearchComponent
      },
      {
        path: "book/:bookId",
        component: UserBookComponent 
      },
      {
        path: "author/:authorId",
        component: UserAuthorComponent 
      },
    ]
},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
