import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { Router } from '@angular/router';
import { UserServices } from 'src/app/shared/services/user.service';

@Component({
  selector: 'user-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class UserHeaderComponent implements OnInit {
  public isSearch: Boolean = false;
  public isEditUser: Boolean = false;
  public userName: String = this.localStorageService.get('user')['user'].userName;
  public email: String = this.localStorageService.get('user')['user'].email;

  public newUserInfo: FormGroup = new FormGroup({
    _id: new FormControl(this.localStorageService.get('user')['user']._id),
    userName: new FormControl(),
    email: new FormControl(),
    password: new FormControl(),
    conf: new FormControl(true),
  })

  constructor(private rout: Router,
    private userServices: UserServices,
    private localStorageService: LocalStorageService) {
  }

  ngOnInit() { }

  changeUserInfo(): void {
    this.userServices.edit(this.newUserInfo.value).subscribe(res => {
      this.localStorageService.set('user', null);
      this.rout.navigate(['']);
    })
  }

  searchOn(): void {
    this.isSearch = !this.isSearch;
    this.isEditUser = false;
  }

  editOn(): void {
    this.isEditUser = !this.isEditUser;
    this.isSearch = false;
  }

  logOut(): void {
    this.localStorageService.remove('user');
    this.rout.navigate([''])
  }


}
