import { Component, OnInit } from '@angular/core';
import { BookModel } from 'src/app/shared/models/books.models';
import { AuthorsModel } from 'src/app/shared/models/authors.models';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { PagingModel } from 'src/app/shared/models/paging.models';
import { BookServices } from '../../../shared/services/book.services';
import { AuthorServices } from 'src/app/shared/services/author.services';
import { UserServices } from 'src/app/shared/services/user.service';


@Component({
  selector: 'user-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class UserSearchComponent implements OnInit {
  public allAuthorsArr: AuthorsModel[];
  public filterallBooksArr: BookModel[];
  public timer = null;
  public searchInputValue: string = '';
  public isLoader: Boolean = false;
  public pagesCounter = this.userServices.pagesCounter();

  public pagingCounter: PagingModel = {
    lengthArr: 0,
    start: 0,
    step: 4,
    quantityBtn: 0
  }

  constructor(private rout: Router,
    private bookServices: BookServices,
    private authorServices: AuthorServices,
    private localStorageService: LocalStorageService,
    private userServices: UserServices) {
    this.getPaging();
  }

  ngOnInit() { }

  public getPaging(): void {
    this.isLoader = true;
    this.bookServices.get().subscribe((res: object[]) => {
      this.pagingCounter.lengthArr = res.length;

      this.bookServices.getPaging(this.pagingCounter).subscribe((res: BookModel[]) => {
        this.filterallBooksArr = res;
        this.isLoader = false;
      })
    })
  }

  public search(): void {
    if (this.timer !== null) {
      clearTimeout(this.timer);
    }
    let id: string = this.localStorageService.get('user')['user']._id;
    this.timer = setTimeout(() => {
      this.bookServices.search(id, this.searchInputValue.toLowerCase(), this.pagingCounter).subscribe((books: any) => {
        this.filterallBooksArr = [];
        this.pagingCounter.lengthArr = books.booksLength;
        this.filterallBooksArr = books.booksArr;
      })
      if (this.searchInputValue.length <= 0) {
        this.pagingCounter.start = 0;
        this.getPaging();
        return;
      }
    }, 500)
  }

  public getPaginationValue(p): void {
    this.pagingCounter.start = p-1;
    this.getPaging();
  }

}
