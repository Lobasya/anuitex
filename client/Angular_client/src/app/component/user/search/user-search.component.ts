
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { UserSearchComponent } from './search.component';



const routes: Routes = [
    { path: "", component: UserSearchComponent},

];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})
export class UserSearchRoutingModule { }