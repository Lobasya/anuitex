import { NgModule } from "@angular/core";
import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserHeaderComponent } from './header/header.component';
import { UserSearchComponent } from './search/search.component';
import { UserBookComponent } from './user-book/user-book.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { UserAuthorComponent } from './author/user-author.component';
import { PreloaderModule } from '../preloader/preloader.module';
import {MatSelectModule} from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UserRoutingModule,
    NgxPaginationModule,
    PreloaderModule,
    MatSelectModule,
    MatFormFieldModule
  ],
  declarations: [
    UserComponent,
    UserSearchComponent,
    UserHeaderComponent,
    UserBookComponent,
    UserAuthorComponent
  ]
})

export class UserModule {}