import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { UserBookComponent } from './user-book.component';
import { UserBookRoutingModule } from './user-book-routing.module';

@NgModule({
  imports: [
    UserBookRoutingModule, 
    CommonModule,
  ],
  declarations: [
    UserBookComponent
  ]
})
export class UserBookModule {}