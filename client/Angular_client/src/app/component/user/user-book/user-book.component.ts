import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookModel } from 'src/app/shared/models/books.models';
import { BookServices } from 'src/app/shared/services/book.services';

@Component({
  selector: 'user-book',
  templateUrl: './user-book.component.html',
  styleUrls: ['./user-book.component.scss']
})
export class UserBookComponent implements OnInit {
  public idPage: string;
  public book: BookModel = {
    title: 'String',
    rexp: 'string',
    img: 'String',
    authorId: [],
    date: new Date(),
    length: 123,
    magazine: []
  };

  public text: String[];

  constructor(private route: ActivatedRoute,
    private bookServices: BookServices) {
    this.getPageId();
    this.getBook();
    this.getText();
  }

  ngOnInit() { }

  public getPageId(): void {
    this.route.params.subscribe(params => {
      this.idPage = params['bookId'];
    });
  }

  public getBook(): void {
    this.bookServices.getById(this.idPage).subscribe((book: BookModel) => {
      this.book = book[0];
      console.log(this.book)
    })
  }

  public getText(): void {
    this.text = this.bookServices.generateRandomText(120, 20)
  }

}
