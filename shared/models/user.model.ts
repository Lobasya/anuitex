import { UserRole } from './userRole.model'

export interface IUserModel {
    _id:any;
    email: string;
    userName: string;
    password: string;
    conf?: boolean; 
    role?: UserRole;
}

