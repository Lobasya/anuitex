export interface UserChanged{
    email: string;
    userName: string;
    password?: string;
}