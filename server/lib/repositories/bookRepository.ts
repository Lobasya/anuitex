import * as mongoose from 'mongoose';
import { IBookModel } from '../../../shared/models/book.model';

const Shema = mongoose.Schema;
const ObjectId = Shema.Types.ObjectId;

interface IBookEntity extends IBookModel, mongoose.Document { }
mongoose.model('Book', new mongoose.Schema({
    title: String,
    img: String,
    authorId: [ObjectId],
    date: Date,
    length: Number,
    magazine: [ObjectId]
}));

export const BookRepository = mongoose.model<IBookEntity>('Book');

