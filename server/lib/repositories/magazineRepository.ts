import * as mongoose from 'mongoose';
import { MagazineModel } from '../../../shared/models/magazine.models';

const Schema = mongoose.Schema;
interface IMagazineEntity extends MagazineModel, mongoose.Document { }
export const MagazineShema = new Schema({
    title: String,
    img: String,
});
export const MagazineRepository = mongoose.model<IMagazineEntity>('Magazine', MagazineShema);