import * as mongoose from 'mongoose';
import { Request, Response } from 'express';
import { RequestModel } from '../middlewares/authMiddleware'
import { BookRepository } from '../repositories/bookRepository';

export class BookController {

    public get(req: RequestModel<{ bookId: string }>, res: Response) {
        BookRepository.find({}, (err, book) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(book);
        });
    }

    public getPaging(req: RequestModel<{ start: number, step: number }>, res: Response) {
        BookRepository.aggregate([
            {
                $lookup:
                {
                    from: "authors",
                    localField: "authorId",
                    foreignField: "_id",
                    as: "authorId"
                }
            },
        ]).then(book => {
            book = book.splice(req.body.start * req.body.step, req.body.step)
            res.json(book)
        })
    }


    public getByAuthorId(req: RequestModel<{ authorId: string }>, res: Response) {
        BookRepository.find({ authorId: req.params.authorId }, (err, book) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(book);
        });
    }

    public getById(req: RequestModel<{ bookId: string }>, res: Response) {
        const objectId = mongoose.Types.ObjectId(req.params.bookId);
        BookRepository.aggregate([
            { $match: { _id: objectId } },
            {
                $lookup:
                {
                    from: "authors",
                    localField: "authorId",
                    foreignField: "_id",
                    as: "authorId"
                }
            }
        ]).then(book => {
            res.json(book)
        })
    }

    public search(req: RequestModel<{ bookId: string }>, res: Response) {
        if (req.body.title.length > 0) {
            let rexp = new RegExp('.*' + req.body.title + '.*', 'i');
            BookRepository.aggregate([
                {
                    $lookup:
                    {
                        from: "authors",
                        localField: "authorId",
                        foreignField: "_id",
                        as: "authorId"
                    }
                },
                { $match: { $or: [{ title: rexp }, { "authorId.name": rexp }] } },
            ]).then(book => {
                let booksLength = book.length;
                book = book.splice(req.body.start * req.body.step, req.body.step)
                res.json({ booksArr: book, booksLength: booksLength })
            })
        }
    }

    public add(req: RequestModel<{}>, res: Response) {
        let newBook = new BookRepository(req.body);
        newBook.save((err, book) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(book);
        });
    }

    public update(req: RequestModel<{ bookId: string }>, res: Response) {
        BookRepository.findOneAndUpdate({ _id: req.params.bookId }, req.body, { new: true }, (err, contact) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(contact);
        });
    }

    public delete(req: RequestModel<{ bookId: string }>, res: Response) {
        BookRepository.remove({ _id: req.params.bookId }, (err) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json({ message: 'Successfully deleted contact!' });
        });
    }

}