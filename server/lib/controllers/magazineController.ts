import { Response } from 'express';
import { RequestModel } from '../middlewares/authMiddleware'
import { MagazineRepository } from '../repositories/magazineRepository';
import { BookRepository } from '../repositories/bookRepository';

export class MagazineController {

    public get(req: RequestModel<{}>, res: Response) {
        MagazineRepository.find({}, (err, author) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(author);
        });
    }

    public getById(req: RequestModel<{ magazineId: string }>, res: Response) {
        MagazineRepository.findById(req.params.magazineId, (err, magazine) => {
            if (err) return res.status(500).send('Error on the server.');
            console.log(magazine)
            res.json(magazine);
        });
    }

    public getPaging(req: RequestModel<{ start: number, step: number }>, res: Response) {
        MagazineRepository.find({}, (err, authors) => {
            authors = authors.splice(req.body.start * req.body.step, req.body.step);
            res.json(authors);
        })
    }

    public search(req: RequestModel<{ title: string, start: number, step: number }>, res: Response) {
        if (req.body.title.length > 0) {
            let rexp = new RegExp('.*' + req.body.title + '.*', 'i');
            MagazineRepository.find({ title: rexp }, ((err, magazines) => {
                let magazinesLength = magazines.length;
                magazines = magazines.splice(req.body.start * req.body.step, req.body.step)
                res.json({ magazinesArr: magazines, magazinesLength: magazinesLength })
            })
            )
        }
    }

    public add(req: RequestModel<{}>, res: Response) {
        let newMagazine = new MagazineRepository(req.body);
        newMagazine.save((err, book) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(book);
        });
    }

    public update(req: RequestModel<{ magazineId: string }>, res: Response) {
        MagazineRepository.findOneAndUpdate({ _id: req.params.magazineId }, req.body, { new: true }, (err, magazine) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(magazine);
        });
    }

    public delete(req: RequestModel<{ magazineId: string }>, res: Response) {
        console.log(req.params)
        BookRepository.updateMany({ magazine: req.params.magazineId }, { $pull: { 'magazine': req.params.magazineId } }, (err, magazine) => {
            if (err) return res.status(500).send('Error on the server.');
            MagazineRepository.deleteOne({ _id: req.params.magazineId }, (err) => {
                if (err) return res.status(500).send('Error on the server.');
                res.json({ message: 'Successfully deleted magazine!' });
            });
        })
    }


}