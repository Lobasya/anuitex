
import { RequestModel, RequestPost } from '../middlewares/authMiddleware';
import { Response } from 'express';
import * as bcryptjs from "bcryptjs";
import * as jsonwebtoken from "jsonwebtoken";
import { authConfig } from "../config"
import { UserRepository } from '../repositories/userRepository';
import { UserRole } from '../../../shared/models/index';

export class AuthController {

    public register(req: RequestPost<{ userName: string, email: string, password: string, conf: boolean }>, res: Response) {
        var hashedPassword = bcryptjs.hashSync(req.body.password, 8);

        UserRepository.create({
            userName: req.body.userName,
            email: req.body.email,
            password: hashedPassword,
            conf: req.body.conf,
            role: UserRole.user
        },
            (err, user) => {
                if (err) return res.status(500).send("There was a problem registering the user.")
                res.status(200).send('User created');
            });
    }

    public login(req: RequestPost<{ email: string, password: string }>, res: Response) {
        UserRepository.findOne({ email: req.body.email }, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            if (!user) return res.status(400).send('No user found.');
            var passwordIsValid = bcryptjs.compareSync(req.body.password, user.password);
            if (!passwordIsValid) return res.status(401).send({ auth: false, token: null });
            var token = jsonwebtoken.sign({ id: user._id, userName: user.userName, email: user.email, role: user.role }, authConfig.secret, {
                expiresIn: 86400 // expires in 24 hours
            });
            res.status(200).send({ auth: true, token: token, user: user });
        });
    }

    public checkRole(req: RequestPost<{ role: number }>, res: Response) {
        res.status(200).json(req['user'].role)
    }

}
