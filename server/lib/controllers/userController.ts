import { RequestPost, RequestModel } from '../middlewares/authMiddleware';
import { Response } from 'express';
import * as bcryptjs from "bcryptjs";
import { UserRepository } from '../repositories/userRepository';
import { UserChanged } from '../../../shared/models';

interface GetByUserId {
    userId: number
}
export class UserController {
    public get(req: RequestModel<{}>, res: Response) {
        UserRepository.find({}, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(user);
        })
    }

    public getPaging(req: RequestModel<{ start: number, step: number }>, res: Response) {
        UserRepository.find({}).then(users => {
            users = users.splice(req.body.start * req.body.step, req.body.step)
            res.json(users)
        })
    }

    public getById(req: RequestModel<GetByUserId>, res: Response) {
        UserRepository.findById(req.params.userId, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(user);
        });
    }

    public updateProfile(req: RequestPost<{ body: Object }>, res: Response) {
        let userForUpdate: UserChanged = {
            userName: req.body['userName'],
            email: req.body['email'],
        };
        if (req.body['password'] !== null) {
            var hashedPassword = bcryptjs.hashSync(req.body['password'], 8);
            Object.assign(userForUpdate, { password: hashedPassword })
        }

        UserRepository.findByIdAndUpdate({ _id: req.body['_id'] }, userForUpdate, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');

            res.status(200).json('User created');
        });
    }


    public search(req: RequestModel<{}>, res: Response) {
        if (req.body.title.length > 0) {
            let rexp = new RegExp('.*' + req.body.title + '.*', 'i');
            UserRepository.aggregate([
                { $match: { $or: [{ userName: rexp }, { email: rexp }] } },
            ]).then(users => {
                let usersLength = users.length;
                users = users.splice(req.body.start * req.body.step, req.body.step)
                res.json({ booksArr: users, booksLength: usersLength })
            })
        }
    }
    
    public update(req: RequestPost<{ userId: string }>, res: Response) {
        UserRepository.findByIdAndUpdate({ _id: req.params.userId }, req.body, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.status(200).send(user);
        });
    }

    public delete(req: RequestPost<{ userId: string }>, res: Response) {
        UserRepository.findByIdAndDelete({ _id: req.params.userId }, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.status(200).send(user);
        });
    }
}
