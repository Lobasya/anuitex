import { RequestModel, RequestPost } from '../middlewares/authMiddleware';
import { Response } from 'express';
import * as bcryptjs from "bcryptjs";
import { UserRepository } from '../repositories/userRepository';


export class AdminControler {

    public confirm(req: RequestPost<{ id: string, conf: boolean }>, res: Response) {
        UserRepository.findByIdAndUpdate({ _id: req.body.id }, { conf: !req.body.conf }, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.status(200).send(user);
        });
    }

    public changeRole(req: RequestPost<{ id: string, role: number }>, res: Response) {
        UserRepository.findByIdAndUpdate({ _id: req.body.id }, { role: req.body.role }, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.status(200).send(user);
        });
    }

    public registerAdmin(req: RequestPost<{ userName: string, email: string, password: string, conf: boolean, role: number }>, res: Response) {
        var hashedPassword = bcryptjs.hashSync(req.body.password, 8);

        UserRepository.create({
            userName: req.body.userName,
            email: req.body.email,
            password: hashedPassword,
            conf: req.body.conf,
            role: req.body.role
        },
            (err, user) => {
                if (err) return res.status(500).send("There was a problem registering the user.")
                res.status(200).send(user);
            });
    }



}
