import { Response } from 'express';
import { RequestModel } from '../middlewares/authMiddleware'
import { AuthorRepository } from '../repositories/authorRepository';
import { BookRepository } from '../repositories/bookRepository';

export class AuthorController {

    public get(req: RequestModel<{}>, res: Response) {
        AuthorRepository.find({}, (err, author) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(author);
        });
    }

    public getById(req: RequestModel<{authorId: String}>, res: Response) {
        AuthorRepository.findById(req.params.authorId, (err, author) => {
            
            if (err) return res.status(500).send('Error on the server.');
            res.json(author);
        });
    }

    public getPaging(req: RequestModel<{ start: number, step: number }>, res: Response) {
        AuthorRepository.find({}, (err, authors) => {
            authors = authors.splice(req.body.start * req.body.step, req.body.step);
            res.json(authors);
        })
    }
    
    public search(req: RequestModel<{ title: string, start: number, step: number }>, res: Response) {
        if (req.body.title.length > 0) {
            let rexp = new RegExp('.*' + req.body.title + '.*', 'i');
            AuthorRepository.find({ name: rexp }, ((err, authors) => {
                let authorsLength = authors.length;
                authors = authors.splice(req.body.start * req.body.step, req.body.step)
                res.json({ authorsArr: authors, authorsLength: authorsLength })
            })
            )
        }
    }
    
    public add(req: RequestModel<{ name: String, img: String }>, res: Response) {
        let newAuthor = new AuthorRepository({ name: req.body.name, img: req.body.img });
        newAuthor.save((err, author) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(author);
        });
    }

    public update(req: RequestModel<{authorId: String}>, res: Response) {
        AuthorRepository.findOneAndUpdate({ _id: req.params.authorId }, req.body, { new: true }, (err, author) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(author);
        });
    }

    public delete(req: RequestModel<{authorId: String}>, res: Response) {
        BookRepository.updateMany({ authorId: req.params.authorId }, { $pull: { 'authorId':  req.params.authorId } }, (err, bo) => {
            if (err) return res.status(500).send('Error on the server.');
            console.log(bo)
            AuthorRepository.remove({ _id: req.params.authorId }, (err) => {
                if (err) return res.status(500).send('Error on the server.');
                BookRepository.find
                res.json({ message: 'Successfully deleted contact!' });
            });
        })
        
    }


}