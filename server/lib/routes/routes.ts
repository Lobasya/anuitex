import { Request, Response, NextFunction } from "express";
import { BookController } from "../controllers/bookController";
import { AuthorController } from "../controllers/authorController";
import { AuthMiddleware } from "../middlewares/authMiddleware";
import { AuthController } from "../controllers/authController"
import { UserRole } from "../../../shared/models";
import { AdminControler } from "../controllers/adminController";
import { UserController } from "../controllers/userController";
import { MagazineController } from "../controllers/magazineController";

export class Routes {

    public bookController: BookController = new BookController()
    public authorController: AuthorController = new AuthorController()
    public authController: AuthController = new AuthController()
    public AdminControler: AdminControler = new AdminControler()
    public UserController: UserController = new UserController()
    public magazineController: MagazineController = new MagazineController()

    public routes(app): void {

        app.route('/')
            .get((req: Request, res: Response) => {
                res.status(200).send({
                    message: 'GET request successfulll!!!!'
                })
            })

        // Auth
        app.route('/auth/register')
            .post(this.authController.register);
        app.route('/auth/login')
            .post(this.authController.login);
        app.route('/auth/user-role')
            .get(AuthMiddleware([UserRole.user, UserRole.admin]), this.authController.checkRole);


        //Admin
        app.route('/admin')
            .get(AuthMiddleware([UserRole.admin]), this.UserController.get)
            .post(AuthMiddleware([UserRole.admin]), this.AdminControler.registerAdmin);
        app.route('/admin/changerol')
            .post(AuthMiddleware([UserRole.admin]), this.AdminControler.changeRole);
        app.route('/admin/confirm')
            .post(AuthMiddleware([UserRole.admin]), this.AdminControler.confirm);

        //User
        app.route('/user')
            .post(AuthMiddleware([UserRole.user, UserRole.admin]), this.UserController.updateProfile);
        app.route('/user/paging')
            .post(AuthMiddleware([UserRole.admin]), this.UserController.getPaging);
        app.route('/user/search')
            .post(AuthMiddleware([UserRole.admin]), this.UserController.search);
        app.route('/user/:userId')
            .get(AuthMiddleware([UserRole.admin]), this.UserController.getById)
            .put(AuthMiddleware([UserRole.admin]), this.UserController.update)
            .delete(AuthMiddleware([UserRole.admin]), this.UserController.delete);

        // Book 
        app.route('/book')
            .get(AuthMiddleware([UserRole.admin, UserRole.user]), this.bookController.get)
            .post(AuthMiddleware([UserRole.admin]), this.bookController.add);
        app.route('/book/paging')
            .post(AuthMiddleware([UserRole.admin, UserRole.user]), this.bookController.getPaging);
        app.route('/book/search')
            .post(AuthMiddleware([UserRole.admin, UserRole.user]), this.bookController.search);
        app.route('/book/author/:authorId')
            .get(AuthMiddleware([UserRole.user]), this.bookController.getByAuthorId);
        app.route('/book/:bookId')
            .get(AuthMiddleware([UserRole.admin, UserRole.user]), this.bookController.getById)
            .put(AuthMiddleware([UserRole.admin, UserRole.user]), this.bookController.update)
            .delete(AuthMiddleware([UserRole.admin]), this.bookController.delete)

        // Author 
        app.route('/author')
            .get(AuthMiddleware([UserRole.admin, UserRole.user]), this.authorController.get)
            .post(AuthMiddleware([UserRole.admin]), this.authorController.add);
        app.route('/author/paging')
            .post(AuthMiddleware([UserRole.admin, UserRole.user]), this.authorController.getPaging);
        app.route('/authors/search')
            .post(AuthMiddleware([UserRole.admin, UserRole.user]), this.authorController.search);
        app.route('/author/:authorId')
            .get(AuthMiddleware([UserRole.user, UserRole.admin]), this.authorController.getById)
            .put(AuthMiddleware([UserRole.admin]), this.authorController.update)
            .delete(AuthMiddleware([UserRole.admin]), this.authorController.delete)


        //Magazines
        app.route('/magazine')
            .get(AuthMiddleware([UserRole.admin, UserRole.user]), this.magazineController.get)
            .post(AuthMiddleware([UserRole.admin]), this.magazineController.add);
        app.route('/magazine/paging')
            .post(AuthMiddleware([UserRole.admin, UserRole.user]), this.magazineController.getPaging);
        app.route('/magazine/search')
            .post(AuthMiddleware([UserRole.admin, UserRole.user]), this.magazineController.search)
        app.route('/magazine/:magazineId')
            .get(AuthMiddleware([UserRole.admin, UserRole.user]), this.magazineController.getById)
            .put(AuthMiddleware([UserRole.admin, UserRole.user]), this.magazineController.update)
            .delete(AuthMiddleware([UserRole.admin]), this.magazineController.delete)

    }
}